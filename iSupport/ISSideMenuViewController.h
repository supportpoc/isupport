//
//  ISSideMenuViewController.h
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
@interface ISSideMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>

@property (strong, nonatomic)IBOutlet UITableView *tableView;

- (IBAction)signOutBtnAction:(id)sender;
- (IBAction)settingsBtnAction:(id)sender;

@end
