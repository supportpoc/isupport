//
//  ISWebViewController.m
//  iSupport
//
//  Created by Upakul on 04/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import "ISWebViewController.h"
#import "GMSpinKitView.h"
#import "RESideMenu.h"

@interface ISWebViewController () <UIWebViewDelegate>
@property (nonatomic, strong) GMSpinKitView *spinner;

@end

@implementation ISWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /* Activity Indicator */
    
    if (self.isFromFeedback || self.isFromLocate)
    {
        UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(showMenu:)];
        self.navigationItem.leftBarButtonItem = menuButton;
    }
    
    _spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:25.0/255.0 green:112.0/255.0 blue:223.0/255.0 alpha:0.8f]];
    _spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    [_spinner startAnimating];
    
    [self.view addSubview:_spinner];
    
    self.navigationItem.title = self.webTitle;

    [self.webView setDelegate:self];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webURL]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    [_spinner stopAnimating];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_spinner stopAnimating];
}

- (void)showMenu:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
