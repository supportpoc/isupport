//
//  ViewController.m
//  CAS
//
//  Created by Sonali on 09/05/14.
//  Copyright (c) 2014 Sonali. All rights reserved.
//

#import "CASRootViewController.h"
#import "CASChatViewController.h"

@interface CASRootViewController ()


@end

@implementation CASRootViewController
BOOL animating;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
   


    self.title = @"Apple Support";
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    animating = YES;
    [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    
}

- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 2 seconds
    [UIView animateWithDuration: 0.4f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         self.imageView.transform = CGAffineTransformRotate(_imageView.transform, M_PI / 2);
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animating) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveLinear) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             }
                         }
                     }];
}



- (IBAction) startStopSpin :(id)sender  {
    if ([sender isSelected]) {
        // set the flag to stop spinning after one last 90 degree increment
        animating = NO;
        [sender setSelected:NO];
    }else{
        if (!animating) {
            animating = YES;
            [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
        }
    
    [sender setSelected:YES];

    }
    
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    animating = NO;
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (IBAction)dismissController:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
