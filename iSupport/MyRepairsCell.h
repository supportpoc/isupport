//
//  MyRepairsCell.h
//  iSupportTest
//
//  Created by Varun on 28/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRepairsCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *productNumber;
@property (strong, nonatomic) IBOutlet UILabel *totalDevices;
@property (strong, nonatomic) IBOutlet UIScrollView *myRepairsScrollView;


@end
