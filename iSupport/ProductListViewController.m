//
//  CEProductListViewController.m
//  CustomerExperience
//
//  Created by Ila on 27/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductTableViewCell.h"
#import "RESideMenu.h"
#import "MGSwipeButton.h"
#import "ISSearchCaseViewController.h"

#define TEST_USE_MG_DELEGATE 1


@interface ProductListViewController ()


@property(nonatomic, strong) NSMutableArray *devices;

@end

@implementation ProductListViewController
@synthesize isPushed;


- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor  = [UIColor colorWithRed:233.0/255 green:233.0/255.0 blue:233.0/255.0 alpha:1];
//    productTable.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1];
//   self.devices = [[NSMutableArray alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DashboardPlist" ofType:@"plist"];
    NSDictionary *myProducts = [NSDictionary dictionaryWithContentsOfFile:path];
   
    NSArray *deviceList = [myProducts objectForKey:@"MyDevices"];
    self.devices = [[NSMutableArray alloc]initWithArray:deviceList];

    
   
    
        /* Navigation Bar */
    
    self.navigationItem.title = @"Device List";
    if(!self.isPushed){
    UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(showMenu:)];
self.navigationItem.leftBarButtonItem = menuButton;
    }
    
    UIImage *addImage = [UIImage imageNamed:@"menu-add-devices.png"];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithImage:addImage
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(addDevice:)];
    self.navigationItem.rightBarButtonItem = addButton;

    
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
//
}

- (void)showMenu:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

- (void)addDevice:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])  {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = nil;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else{
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Camera facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [calert show];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//        return [self.details count];
    return [self.devices count];

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *device =  [self.devices objectAtIndex:indexPath.row];
   
       static NSString *popIdentifier = @"CEProductTableViewCell";
    self.productCell = [tableView dequeueReusableCellWithIdentifier:popIdentifier];
    [self.productCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if(self.productCell == nil){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ProductTableViewCell" owner:self options:nil];
        self.productCell = [nib objectAtIndex:0];
    }
    self.productCell.productImage.image = [UIImage imageNamed:[device objectForKey:@"productImage"]];
    self.productCell.productName.text = [device objectForKey:@"productName"];
    [self.productCell.serialNo setText:[NSString stringWithFormat:@"Serial Number: %@",device[@"serialNumber"]]];
    self.productCell.tecnicalSupportExpDate.text = [device objectForKey:@"tecnicalSupportExpDate"];
    self.productCell.repairServiceExpDate.text = [device objectForKey:@"repairServiceExpDate"];
    
    if([[device objectForKey:@"tecnicalSupportExpDate"] isEqualToString:@"Expired"]){
        [self.productCell.supportStatusImage  setImage:[UIImage imageNamed:@"invalid-icon"]];
    }
    else{
        [self.productCell.supportStatusImage  setImage:[UIImage imageNamed:@"valid-icon"]];
    }
    
    if([[device objectForKey:@"repairServiceExpDate"] isEqualToString:@"Expired"]){
        [self.productCell.repairStatusImage  setImage:[UIImage imageNamed:@"invalid-icon"]];
    }
    else{
        [self.productCell.repairStatusImage  setImage:[UIImage imageNamed:@"valid-icon.png"]];
    }

    self.productCell.delegate = self;
    self.productCell.rightButtons = [self createRightButtons:1];
    return self.productCell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [self showGrid];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

-(BOOL) swipeTableCell:(ProductTableViewCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        NSIndexPath * path = [productTable indexPathForCell:cell];
        [self.devices removeObjectAtIndex:path.row];
        [productTable deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
    }
    
    return YES;
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    NSString* titles = @"Delete";
    UIColor * colors = [UIColor redColor];
    for (int i = 0; i < number; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles backgroundColor:colors callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (right).");
            return YES;
        }];
        [result addObject:button];
    }
    return result;
}

- (void)showGrid {
    NSInteger numberOfOptions = 4;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"create-a-case"] title:@"Create A Case"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"setup-a-repair"] title:@"Setup A Repair"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"take-in-service-circle"] title:@"Take In Service"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"extend-service-support-coverage"] title:@"Extend Support"],
                       
                       ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    
    // RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:161.0/255.0 blue:235.0/255.0 alpha:1.0 ];
    av.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    av.itemSize = CGSizeMake(150,150);
    av.blurLevel = 0.1;
    // av.itemFont = [UIFont boldSystemFontOfSize:10];
    
    //    av.bounces = NO;
    //[av showViewController:self sender:nil];
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex
{
    if (itemIndex == 0)
    {
        ISSearchCaseViewController *createCaseViewController = (ISSearchCaseViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"ISSearchCaseViewController"];

        [self presentViewController:createCaseViewController animated:YES completion:^{
            
        }];
    }
    
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
}



@end
