
//
//  CASChatViewController.m
//  CAS
//
//  Created by Sonali/Rakesh on 09/05/14.
//  Copyright (c) 2014 Apple. All rights reserved.
//


#import "CASChatViewController.h"
#import "CASChatData.h"
#import "CASBubbleCell.h"
#import "ImageCell.h"
#import "ChatCollectionHeaderView.h"
#import "ChatCollectionViewFooterView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "CASUtility.h"

@interface CASChatViewController () {
    NSMutableArray * chatData; // array to store chat messages
    UIView *containerView;
    HPGrowingTextView *textView;
}
@end

#define kMaxMessageWidth 250
#define kMaxHeight 10000.f
#define minimumHeight 25
#define dateFormatFor @"MM/dd/YY hh:mm a"
#define textViewfont [UIFont fontWithName:@"HelveticaNeue" size:18]
#define messageByMe @"textbyme"
#define messageByOther @"textbyother"


static NSString *CellIdentifier = @"Cell";
static NSString *ImageCellIdentifier = @"ImageCell";


@implementation CASChatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    self.title = @"Apple Support";

    chatData = [NSMutableArray array];
    
    //Registering Collection view cells
    [self.chatCollectionView registerNib:[UINib nibWithNibName:@"CASBubbleCell" bundle:nil] forCellWithReuseIdentifier:CellIdentifier];
    [self.chatCollectionView registerNib:[UINib nibWithNibName:@"ImageCell" bundle:nil] forCellWithReuseIdentifier:ImageCellIdentifier];
    [self.chatCollectionView registerClass:[ChatCollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self.chatCollectionView registerClass:[ChatCollectionViewFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
    
    
    //Add end Chat button
//    UIBarButtonItem * endChatBtn = [[UIBarButtonItem alloc] initWithTitle:@"End Chat" style:UIBarButtonItemStylePlain target:self action:@selector(endChat:)];
    UIBarButtonItem * endChatBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chat-end-icon"] landscapeImagePhone:[UIImage imageNamed:@"chat-end-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(endChat:)];
    [self.navigationItem setRightBarButtonItem:endChatBtn];
    
    
    //Add send bar at bottom
    [self setUpTextFieldforIphone];
    
    
    //Keybord notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // Adding dummy messages
    [self setUpDummyMessages];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setUpDummyMessages
{
    NSDate *date = [NSDate date];
    
    //NSString *rowNumber=[NSString stringWithFormat:@"%lu",(unsigned long)chatBubbledata.count];
    
    
    [self adddBubbledata:messageByOther mtext:@"Greetings from Apple how can we help you today?" mtime:date];
    [self adddBubbledata:messageByMe mtext:@"I have having issues with my iPhone, can you please help?" mtime:date ];
    [self adddBubbledata:messageByOther mtext:@"Yes sure, what is the issue?" mtime:date ];
    [self adddBubbledata:messageByMe mtext:@"I am unable to power off or shut down my iPhone." mtime:date ];
    [self adddBubbledata:messageByOther mtext:@"An iOS restore is likely to resolve the issue. have you tried the same?" mtime:date ];
    [self adddBubbledata:messageByMe mtext:@"Yes , I did but it doesn't resolve the issue." mtime:date];
    [self adddBubbledata:messageByOther mtext:@"Let us know your serial number and system information. It will be used to help resolve the issue more quicikly" mtime:date];
    [self adddBubbledata:messageByMe mtext:@"My iPhone serial number is XYXYXYXYXYXYXYXYXYXYX." mtime:date ];
    [self adddBubbledata:messageByOther mtext:@"Thanks for providing the information. We wil look into the issue and will let you know." mtime:date ];
    
}


#pragma mark Collection View Delegates
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else
    return  [chatData count] ;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize size;
    
    if (indexPath.section == 0) {
        size =  CGSizeMake(320, 295);
        
    }else{
    
    CASChatData *chatMessage = [chatData objectAtIndex:indexPath.row];
    CGRect textRect = [CASUtility getDynamicRectForText:chatMessage.messageText maxTextWidth:kMaxMessageWidth maxTextHeight:kMaxHeight textFont:textViewfont];
    
    float cellHeight = textRect.size.height;
    if (cellHeight < minimumHeight) {
        cellHeight = minimumHeight;
    }
    
    size =  CGSizeMake(320, cellHeight+15);
    }
    
    return size;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(320,0);
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
       if (section == [collectionView numberOfSections] -1)
    return CGSizeMake(320,50);
    else
        return CGSizeMake(0, 0);
    
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CASChatData *chatMessage = [chatData objectAtIndex:indexPath.row];
    CGRect textRect = [CASUtility getDynamicRectForText:chatMessage.messageText maxTextWidth:kMaxMessageWidth maxTextHeight:kMaxHeight textFont:textViewfont];
    
    float textWidth = textRect.size.width + 10;
    if (textWidth > kMaxMessageWidth) {
        textWidth = kMaxMessageWidth;
    }
    
    float textHeight = textRect.size.height + 20;
    
    float bubbleOffsetWithRespectToTextView = 10;
    float bubbleAndTextViewOffsetWithRespectToCell = 15;
    
    UICollectionViewCell * cell;
    
    if (indexPath.section == 0) {
        
        cell = nil;
        if (cell == nil) {
            cell = (ImageCell *)[self.chatCollectionView dequeueReusableCellWithReuseIdentifier:ImageCellIdentifier forIndexPath:indexPath];
            
        }
        
        return cell;
    }
    else{

        cell = nil;
        if (cell == nil) {
            cell = (CASBubbleCell *)[self.chatCollectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
            
        }
        
    UIImageView *bubbleImage = (UIImageView*)[cell viewWithTag:50];
    UITextView *messageTextview = (UITextView*)[cell viewWithTag: 150];
    messageTextview.font = textViewfont;
    messageTextview.text = chatMessage.messageText;
    
    if ([chatMessage.messageType isEqualToString:messageByMe]) {
    
        
        [bubbleImage setImage:[[UIImage imageNamed:@"Bubbletyperight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
        [bubbleImage setFrame:CGRectMake(320-textWidth-bubbleOffsetWithRespectToTextView-bubbleAndTextViewOffsetWithRespectToCell,0,
                                         textWidth+2*bubbleOffsetWithRespectToTextView,textHeight)];
        
        messageTextview.textColor=[UIColor whiteColor];
        messageTextview.frame = CGRectMake(320-textWidth-bubbleAndTextViewOffsetWithRespectToCell,0,textWidth,textHeight);
        
    }  else {
        [bubbleImage setImage:[[UIImage imageNamed:@"Bubbletypeleft"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
        [bubbleImage setFrame:CGRectMake(bubbleAndTextViewOffsetWithRespectToCell-bubbleOffsetWithRespectToTextView,0,
                                         textWidth+2*bubbleOffsetWithRespectToTextView,textHeight)];
        
        
        messageTextview.textColor=[UIColor blackColor];
        messageTextview.frame = CGRectMake(bubbleAndTextViewOffsetWithRespectToCell,0,textWidth,textHeight);
    }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}


#pragma mark Add Data
-(void)adddBubbledata:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSDate*)messageTime
{
    NSString * dateString = [[CASUtility sharedUtility] yearMonthDayTimestampNumericStringFromNumericDate:messageTime];
    
    CASChatData *feed_data=[[CASChatData alloc]init];
    feed_data.messageText= messagetext;
    feed_data.messageTime=dateString;
    feed_data.messageType=messageType;
    
    [chatData addObject:feed_data];
}


#pragma mark - Send Bar

-(void)setUpTextFieldforIphone
{
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-40, 320, 40)];
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(55, 3, 230, 40)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 6;
	textView.returnKeyType = UIReturnKeyDefault; //just as an example
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor clearColor];
    
    // textView.text = @"test\n\ntest";
	// textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
	
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(55, 0,230, 40);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
    [containerView addSubview:entryImageView];
    
    UIImage *sendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
    UIImage *camBtnBackground = [[UIImage imageNamed:@"camera.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    
    
    UIImage *selectedSendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
	UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(containerView.frame.size.width - 30, 8, 25, 25);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	//[doneBtn setTitle:@"send" forState:UIControlStateNormal];
    
    [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    [doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:doneBtn];
    
    
    
    UIButton *doneBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn2.frame = CGRectMake(containerView.frame.origin.x+10,10, 30,20);
    doneBtn2.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
	[doneBtn2 setTitle:@"" forState:UIControlStateNormal];
    
    [doneBtn2 setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn2.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn2.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn2 addTarget:self action:@selector(uploadImageOrVideo:) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn2 setBackgroundImage:camBtnBackground forState:UIControlStateNormal];
    
    //[doneBtn2 setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
    
	[containerView addSubview:doneBtn2];
    
    
    
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    
}


#pragma mark - Keybord Notifications
-(void) keyboardWillShow:(NSNotification *)note
{
    
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    CGRect tableviewframe=self.chatCollectionView.frame;
    tableviewframe.size.height-=160;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	containerView.frame = containerFrame;
    self.chatCollectionView.frame=tableviewframe;
    
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    CGRect tableviewframe=self.chatCollectionView.frame;
    tableviewframe.size.height+=160;
    
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	// set views with new info
    self.chatCollectionView.frame=tableviewframe;
	containerView.frame = containerFrame;
	// commit animations
	[UIView commitAnimations];
}

#pragma mark - TextView Delegates
-(void)resignTextView{
    [textView resignFirstResponder];
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView{
    if ([textView.text length]>0) {
        UIButton * doneBtn =  (UIButton *)[containerView viewWithTag:100];
        [doneBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
    }
}

- (void)growingTextViewDidEndEditing:(HPGrowingTextView *)growingTextView{
    UIButton * doneBtn =  (UIButton *)[containerView viewWithTag:100];
    [doneBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}



- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
}

-(NSString*)formatTheDate:(NSDate *)inputdate
{
    NSString *retstring;
    
    if( [CASUtility isToday:inputdate] )
    {
        retstring=@"Today";
        
        retstring = [retstring stringByAppendingFormat:@" %@",[[CASUtility sharedUtility] HourMinuteFromDate:inputdate]];
    }
    else if( [CASUtility isYesterday:inputdate] )
    {
        retstring=@"Yesterday";
        retstring = [retstring stringByAppendingFormat:@" %@",[[CASUtility sharedUtility] HourMinuteFromDate:inputdate]];

    }
    else if( [CASUtility is2to7daysAgo:inputdate] )
    {
        retstring= [CASUtility GetDayOfWeekString:inputdate];
        retstring = [retstring stringByAppendingFormat:@" %@",[[CASUtility sharedUtility] HourMinuteFromDate:inputdate]];

    }
    else
    {
        retstring = [[CASUtility sharedUtility] monthDayYearTimeNumericStringFromNumericDate:inputdate];
    }
    return retstring;
}


#pragma mark - End Chat

-(IBAction)endChat:(id)sender{
  //  [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];    
}

#pragma mark - UploadVideoOrImage

-(IBAction)uploadImageOrVideo:(id)sender{
    UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo or Video", @"Choose existing", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =
                UIImagePickerControllerSourceTypeCamera;
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
            break;
        case 1:
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =
                UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
            break;
        case 2:
            
            break;
            
        default:
            break;
    }
    
}



@end
