//
//  RepairListVIewControllerViewController.h
//  iSupport
//
//  Created by Varun on 03/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepairTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "RNGridMenu.h"

@interface RepairListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MGSwipeTableCellDelegate,RNGridMenuDelegate>
{
    IBOutlet UITableView *repairTable;
}

@property(strong,nonatomic)RepairTableViewCell *repairCell;
@property(assign,nonatomic) NSInteger *isPushed;


@end




