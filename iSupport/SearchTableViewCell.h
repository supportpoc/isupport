//
//  SearchTableViewCell.h
//  iSupport
//
//  Created by Ankur on 01/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell{

    // TableCell
    
    
    
}

@property(nonatomic, strong) IBOutlet UILabel *titleLabel;
@property(nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property(nonatomic, strong) IBOutlet UIButton *locationBtn;
@property(nonatomic, strong) IBOutlet UIButton *globeBtn;

@end
