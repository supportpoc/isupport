//
//  MyDevicesView.h
//  iSupportTest
//
//  Created by Varun on 28/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDevicesView : UIView

@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *serialNumber;
@property (strong, nonatomic) IBOutlet UILabel *tecnicalSupportExpDate;
@property (strong, nonatomic) IBOutlet UILabel *repairServiceExpDate;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UIImageView *supportStatusImage;
@property (strong, nonatomic) IBOutlet UIImageView *repairStatusImage;

@end
