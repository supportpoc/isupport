//
//  ISMenuCollectionView.m
//  iSupport_PROT_1
//
//  Created by Ankit on 03/06/14.
//  Copyright (c) 2014 Ankit. All rights reserved.
//

#import "ISMenuCollectionView.h"
#import "ISMenuCollectionViewCell.h"


@implementation ISMenuCollectionView

#pragma mark - reload data
-(void) reloadData{
    self.menuOptionsArray =  [self.menuOptionsDatasource  menuOptiosArrayForCollectionView:self];;
    self.menuIconsArray = [self.menuOptionsDatasource menuIconsArrayForCollectionView:self];
    [super reloadData];
}

-(void) setupMenuOptionsWithArray{
//    self.menuOptionsArray = [NSArray arrayWithObjects:@"Manuals",
//                             @"Locate Apple Service Centers",
//                             @"Video Tutorials",
//                             @"Apple Support Communities",
//                             @"Tech Specs",
//                             @"Contact Apple Support",
//                             @"Download",
//                             @"Check Service & Support Coverage",
//                             @"Check Warranty & Support",
//                             @"Take in Service",
//                             @"Check Repair Status",
//                             @"Replacement Program", nil];;
//    
//    self.menuIconsArray = [NSArray arrayWithObjects:@"manuals.png",
//                           @"locate-apple-service-centers.png",
//                           @"video-tutorials.png",
//                           @"apple-support-communities.png",
//                           @"tech-specs.png",
//                           @"contact-apple-support.png",
//                           @"manuals.png",
//                           @"schedule-a-call.png",
//                           @"schedule-a-call.png",
//                           @"tech-specs.png",
//                           @"chat-with-executive.png",
//                           @"worldwide-support-numbers@2x.png",nil];
    
    self.delegate = self;
    self.dataSource = self;
}
-(void) setupCollectionView{
    [self registerClass:[ISMenuCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self setPagingEnabled:YES];
    [self setCollectionViewLayout:flowLayout];
    
    pageCount = ceilf([self.menuOptionsArray count]/6.0);
    self.pageControl.numberOfPages = 2;
    
}

-(id) initWithMenuOptions:(NSArray*) menuOptions{
    self = [super init];
    if(self){
//        [self setupMenuOptionsWithArray:menuOptions];
//        [self setupCollectionView];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupMenuOptionsWithArray];
        [self setupCollectionView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupMenuOptionsWithArray];
        [self setupCollectionView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - collectionview datasource/delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.menuOptionsArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ISMenuCollectionViewCell *cell = (ISMenuCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    NSString *imageName = [self.menuIconsArray objectAtIndex:indexPath.row];
    [cell setImageName:imageName];
    [cell.menuOptionLabel setText:[self.menuOptionsArray objectAtIndex:indexPath.row]];
    
    [cell updateCell];
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.menuOptionsDelegate menuCollectionView:self selectedItemAtIndexpath:indexPath];
}

#pragma mark - flowlayout delegate
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(106.67, 80);
}

#pragma mark - scroll view delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender{
    NSLog(@"worked");
    if (!pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.frame.size.width;
		int page = floor((self.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

#pragma mark - page control
- (IBAction)changePage{
    CGRect frame;
	frame.origin.x = self.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.frame.size;
	[self scrollRectToVisible:frame animated:YES];
	pageControlBeingUsed = YES;
}


@end
