//
//  SearchViewController.h
//  iSupport
//
//  Created by Ankur on 28/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController{

    IBOutlet UITextField *searchTF;
    IBOutlet UITableView *searchTableView;
    

}

@end
