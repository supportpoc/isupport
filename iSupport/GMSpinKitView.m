//
//  GMSpinKitView.m
//  xmLabeling
//
//  Created by Upakul on 20/07/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "GMSpinKitView.h"
#include <tgmath.h>

@interface GMSpinKitView ()
@property (nonatomic, assign, getter = isStopped) BOOL stopped;
@end

@implementation GMSpinKitView

-(instancetype)initWithColor:(UIColor*)color {
    self = [super init];
    if (self) {
        _color = color;
        _hidesWhenStopped = YES;
        
        [self sizeToFit];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationWillEnterForeground)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidEnterBackground)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [self.layer setShadowColor:[UIColor lightGrayColor].CGColor];
        [self.layer setShadowRadius:1.5];
        [self.layer setShadowOpacity:1.0];
        [self.layer setShadowOffset:CGSizeMake(0.0, 0.0)];

    NSTimeInterval beginTime = CACurrentMediaTime();
    
    CGSize size = CGSizeMake(self.bounds.size.width, self.bounds.size.height);
    CGFloat squareSize = size.width / 3;
    
    for (NSInteger sum = 0; sum < 5; sum++)
    {
        for (NSInteger x = 0; x < 3; x++)
        {
            for (NSInteger y = 0; y < 3; y++)
            {
                if (x + y == sum)
                {
                    CALayer *square = [CALayer layer];
                    square.frame = CGRectMake(x * squareSize, y * squareSize, squareSize, squareSize);
                    square.backgroundColor = color.CGColor;
                    square.transform = CATransform3DMakeScale(0.0, 0.0, 0.0);
                    
                    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
                    anim.removedOnCompletion = NO;
                    anim.repeatCount = HUGE_VALF;
                    anim.duration = 1.5;
                    anim.beginTime = beginTime + (0.1 * sum);
                    anim.keyTimes = @[@(0.0), @(0.4), @(0.6), @(1.0)];
                    
                    anim.timingFunctions = @[
                                             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]
                                             ];
                    
                    anim.values = @[
                                    [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.0, 0.0, 0.0)],
                                    [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 0.0)],
                                    [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 0.0)],
                                    [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.0, 0.0, 0.0)]
                                    ];
                    
                    [self.layer addSublayer:square];
                    [square addAnimation:anim forKey:@"spinkit-anim"];
                }
            }
        }
    }
    }
//    ==
    return self;
}

-(void)applicationWillEnterForeground {
    if (self.stopped) {
        [self pauseLayers];
    } else {
        [self resumeLayers];
    }
}

-(void)applicationDidEnterBackground {
    [self pauseLayers];
}

-(BOOL)isAnimating {
	return !self.isStopped;
}

-(void)startAnimating {
	if (self.isStopped) {
		self.hidden = NO;
		self.stopped = NO;
		[self resumeLayers];
	}
}

-(void)stopAnimating {
	if ([self isAnimating]) {
		if (self.hidesWhenStopped) {
			self.hidden = YES;
		}
		
		self.stopped = YES;
		[self pauseLayers];
	}
}

-(void)pauseLayers {
    CFTimeInterval pausedTime = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.layer.speed = 0.0;
    self.layer.timeOffset = pausedTime;
}

-(void)resumeLayers {
    CFTimeInterval pausedTime = [self.layer timeOffset];
    self.layer.speed = 1.0;
    self.layer.timeOffset = 0.0;
    self.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.layer.beginTime = timeSincePause;
}

-(CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(48.0, 48.0);
}

-(void)setColor:(UIColor *)color {
    _color = color;
    
    for (CALayer *l in self.layer.sublayers) {
        l.backgroundColor = color.CGColor;
    }
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

