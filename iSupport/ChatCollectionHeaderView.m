//
//  ChatCollectionHeaderView.m
//  CAS
//
//  Created by Sonali on 09/05/14.
//  Copyright (c) 2014 Sonali. All rights reserved.
//

#import "ChatCollectionHeaderView.h"
#import "Constant.h"

@implementation ChatCollectionHeaderView
@synthesize title;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 320, 50)];
        self.title.font = headerViewfont;
        [self.title setTextAlignment:NSTextAlignmentCenter];
        self.title.textColor    = [UIColor darkGrayColor];
        [self addSubview:self.title];
    }
    return self;
}
@end
