//
//  ISDashboardViewController.m
//  iSupport
//
//  Created by Asif on 27/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import "ISDashboardViewController.h"
#import "ISRootViewController.h"
#import "ISWebViewController.h"
#import "DeviceDashboardController.h"
#import "ACMAppleConnect.h"
#import "MSAAppleConnect.h"
#import "MapViewController.h"
#import "AppDelegate.h"


@interface ISDashboardViewController ()
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSArray *serviceCentersArray;

@end

@implementation ISDashboardViewController
@synthesize wrap;
@synthesize mCarousel;

-(void) setUp{
    wrap = YES;
    //carousel.type = i;
    self.carouselItems = [NSMutableArray arrayWithObjects:@"iMac", @"iPhone5", @"iPad", @"iPod", nil];
    
    self.serviceCentersArray = ((AppDelegate *)[UIApplication sharedApplication].delegate).storesArray;

    [UIView beginAnimations:nil context:nil];
    mCarousel.type = iCarouselTypeRotary;
    [UIView commitAnimations];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //[self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setUp];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc{
    mCarousel.delegate = nil;
    mCarousel.dataSource = nil;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.carouselItems count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UIImageView *iconImageView = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        //((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeCenter;
        
        iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(40, 10, 120, 150)];
        iconImageView.tag = 2;
        
        iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(50, 130, 100, 100)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.font = [label.font fontWithSize:15];
        label.tag = 1;
        [view addSubview:label];
        [view addSubview:iconImageView];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
        iconImageView = (UIImageView*)[view viewWithTag:2];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = self.carouselItems[index] ;
    
    iconImageView.image =[UIImage imageNamed:[NSString stringWithFormat:@"%@-icon.png",self.carouselItems[index]]];
    //[iconImageView setBackgroundColor:[UIColor yellowColor]];
    
    
    [view setBackgroundColor:[UIColor whiteColor]];
    [view.layer setBorderColor:[[UIColor grayColor] CGColor]];
    [view.layer setBorderWidth:1.0];
    view.layer.cornerRadius = 100.0;
    [view setClipsToBounds:YES];
    return view;
}

- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

//- (UIView *)carousel:(iCarousel *)carousel placeholderViewAtIndex:(NSUInteger)index reusingView:(UIView *)view
//{
//    UILabel *label = nil;
//    
//    //create new view if no view is available for recycling
//    if (view == nil)
//    {
//        //don't do anything specific to the index within
//        //this `if (view == nil) {...}` statement because the view will be
//        //recycled and used with other index values later
//        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
//        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
//        view.contentMode = UIViewContentModeCenter;
//        
//        
//        label = [[UILabel alloc] initWithFrame:view.bounds];
//        label.backgroundColor = [UIColor clearColor];
//        label.textAlignment = UITextAlignmentCenter;
//        label.font = [label.font fontWithSize:50.0f];
//        label.tag = 1;
//        [view addSubview:label];
//    }
//    else
//    {
//        //get a reference to the label in the recycled view
//        label = (UILabel *)[view viewWithTag:1];
//    }
//    
//    //set item label
//    //remember to always set any properties of your carousel item
//    //views outside of the `if (view == nil) {...}` check otherwise
//    //you'll get weird issues with carousel item content appearing
//    //in the wrong place in the carousel
//    
//    //label.text = (index == 0)? @"[": @"]";
//    
//    return view;
//}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * mCarousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (mCarousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}

#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSNumber *item = (self.carouselItems)[index];
    NSLog(@"Tapped view number: %@ %d", item,index);
    UIView* selectedView = [carousel itemViewAtIndex:index];
    [selectedView setBackgroundColor:[UIColor colorWithRed:0/255.0 green:136/255.0 blue:223/255.0 alpha:1.0]];
    
               [self showGrid];
    
  
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    NSLog(@"sdf");
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel;{
    //[carou]
    
     NSLog(@"ww");
    for (UIView *visibleView in [carousel visibleItemViews]) {
        [visibleView setBackgroundColor:[UIColor whiteColor]];
        UILabel *label  = (UILabel *)[visibleView viewWithTag:1];
        [label setTextColor:[UIColor blackColor]];
        [visibleView.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [visibleView.layer setBorderWidth:1.0];
    }
    UIView* selectedView = [carousel currentItemView];
    [UIView beginAnimations:nil context:nil];
    [selectedView setBackgroundColor:[UIColor colorWithRed:0/255.0 green:136/255.0 blue:223/255.0 alpha:1.0]];
    UILabel *label  = (UILabel *)[selectedView viewWithTag:1];
    [label setTextColor:[UIColor whiteColor]];
    [selectedView.layer setBorderWidth:0.0];
    [UIView commitAnimations];
    
}

- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index{
    NSLog(@"ww");
    return YES;
}

#pragma mark - menu options datasource/delegate

#pragma mark - menu options datasource/delegate

-(NSArray*) menuOptiosArrayForCollectionView:(ISMenuCollectionView *)menuCollectionView{
    return [NSArray arrayWithObjects:
            @"Repair Status",
            
            @"Locate",
            @"Check Service and Support",
            
            @"Replacement Program",
            
            @"Feedback",
            
            
            
            @"Send Diagonistics", nil];;
}
-(NSArray*) menuIconsArrayForCollectionView:(ISMenuCollectionView *)menuCollectionView{
    return [NSArray arrayWithObjects:
            @"menu-setup-repair.png",
            
            @"locate-apple-service-centers.png",
            
             @"menu-create-cases.png",
            
            @"menu-replace.png",
            
            @"feedback-icon.png",
            
           
            
            @"tech-specs",nil];
}



-(void) menuCollectionView:(ISMenuCollectionView *)menuCollectionView selectedItemAtIndexpath:(NSIndexPath *)indexpath{
 
//    NSLog(@"selected item at index %d", indexpath.row);
    switch (indexpath.row) {
        case 1:
        {
//            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//            webViewController.webURL = kLocateURL;
//            webViewController.webTitle = @"Locate";
//            [self.navigationController pushViewController:webViewController animated:YES];
          //  [self loacateAppleServiceCenters];
          MapViewController *mapViewController = (MapViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
         
            mapViewController.mapPinsArray= self.serviceCentersArray;

            
            [self.navigationController pushViewController:mapViewController animated:YES];
            break;

        }

//            [self showManuals];
//            break;
        case 0:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kCheckRepairStatus;
            webViewController.webTitle = @"Check Repair Status";
            webViewController.isFromFeedback = NO;
            
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            //[self loacateAppleServiceCenters];
            break;
        case 2:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kCheckSupport;
            webViewController.webTitle = @"Check Service and Support";
            webViewController.isFromFeedback = NO;
            
            [self.navigationController pushViewController:webViewController animated:YES];

            // [self download];
            break;
        }
        case 3:{
            break;
        }
        case 4:{
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = @"http://www.apple.com/feedback/";
            webViewController.webTitle = @"Send Feedback";
            webViewController.isFromFeedback = NO;
            
            [self.navigationController pushViewController:webViewController animated:YES];
            
            break;

        }
        case 5:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"diags://"]];

            break;
            
        default:
            break;
    }
}

#pragma mark - menu options
-(void) showManuals{
    NSLog(@"show manuals");
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MainStoryBoard" bundle:nil];
//    ISManualViewController *manualVC = [storyBoard instantiateViewControllerWithIdentifier:@"ISManualViewController"];
//    [self.navigationController pushViewController:manualVC animated:YES];
}

-(void) loacateAppleServiceCenters{
    
}

-(void) showVideoTutorials{
    
}

-(void) showAppleSupportCommunities{
    
}

-(void) showTechSpecs{
    
}

-(void)contactAppleSupport{
    
}

-(void) download{
    NSLog(@"download");
}

-(void) checkServiceAndSupportCoverage{
    
}

#pragma -

- (IBAction)signInBtnAction:(id)sender
{
//    [MSAAppleConnect sharedInstance].contr = self;
//    [[MSAAppleConnect sharedInstance] connect];
    

    
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ISRootViewController *rootVC = (ISRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"rootController"];
    [self.navigationController pushViewController:rootVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}


- (void)showGrid {
    NSInteger numberOfOptions = 6;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"contact-apple-support.png"] title:@"Contact Support"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"tech-specs.png"] title:@"Tips"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"apple-support-communities.png"] title:@"Apple Communities"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"manuals.png"] title:@"Manuals"],
                      
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"video-tutorials.png"] title:@"Video Tutorials"],
                      
                       
//                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"download-icon.png"] title:@"Downloads"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"faqs-icon.png"] title:@"FAQs"],
                       
                       ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.tag = [NSNumber numberWithInteger:1];
    // RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:161.0/255.0 blue:235.0/255.0 alpha:1.0 ];
    av.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    av.itemSize = CGSizeMake(150,100);
    av.blurLevel = 0.1;
    av.itemFont = [UIFont boldSystemFontOfSize:12];
    
    //    av.bounces = NO;
    //[av showViewController:self sender:nil];
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

-(void) showSupportGridMenu {
    NSInteger numberOfOptions = 4;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"schedule-a-call.png"] title:@"Call"],
                       [ [RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat-with-executive.png"] title:@"Chat"],
                       [ [RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"menu-my-messages"] title:@"Mail"],
                       [ [RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"contact-a-mobile-carrier.png"] title:@"Call Back"],
                       ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.tag = [NSNumber numberWithInteger:2];
    
    av.delegate = self;
    av.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    av.itemSize = CGSizeMake(150,100);
    av.blurLevel = 0.1;
    av.itemFont = [UIFont boldSystemFontOfSize:12];
    
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
    
    
}

-(IBAction)supportClicked:(id)sender{
    [self showSupportGridMenu];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
    
    if([gridMenu.tag integerValue] == 1){
        [self didSelectFirstGridMenuWithItem:item atIndex:itemIndex];
    }
    else{
        [self didSelectSecondGridMenuWithItem:item atIndex:itemIndex];
    }
    
//    switch (itemIndex) {
//        case 0:
//        {
//            if([gridMenu.tag integerValue] == 1){
//                ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//                if(self.mCarousel.currentItemIndex == 1){
//                    webViewController.webURL = kManualsiPhoneURL;
//                }
//                else if(self.mCarousel.currentItemIndex == 0){
//                    webViewController.webURL = kManualsiPhoneURL;
//                    
//                }
//                else if(self.mCarousel.currentItemIndex == 2){
//                    webViewController.webURL = kManualsiPadURL;
//                    
//                }
//                else if(self.mCarousel.currentItemIndex == 3){
//                    webViewController.webURL = kManualsiPodURL;
//                    
//                }
//                webViewController.webTitle = @"Manuals";
//                [self.navigationController pushViewController:webViewController animated:YES];
//            }
//            else{
//                NSString *phNo = @"+1-800-275-2273";
//                NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
//                
//                if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
//                    [[UIApplication sharedApplication] openURL:phoneUrl];
//                } else
//                {
//                    UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [calert show];
//                }
//                
//            }
//            break;
//        }
//        case 1:
//        {
//            
//            if([gridMenu.tag integerValue] == 1){
//                ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//                webViewController.webURL = kLocateURL;
//                webViewController.webTitle = @"Locate";
//                [self.navigationController pushViewController:webViewController animated:YES];
//            }
//            else{
//                UINavigationController *navigationController  = (UINavigationController*) [self.storyboard instantiateViewControllerWithIdentifier:@"chatnavcontroller"];
//                navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
//                [self presentViewController:navigationController animated:YES completion:nil];
//            }
//            break;
//        }
//        case 2:
//        {
//            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//            webViewController.webURL = kVideoURL;
//            webViewController.webTitle = @"Video Tutorials";
//            [self.navigationController pushViewController:webViewController animated:YES];
//            
//            break;
//        }
//        case 3:
//        {
//            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//            webViewController.webURL = kCommunitiesURL;
//            webViewController.webTitle = @"Communities";
//            [self.navigationController pushViewController:webViewController animated:YES];
//            
//            break;
//        }
//        case 4:
//        {
//            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//            webViewController.webURL = kSpecsURL;
//            webViewController.webTitle = @"Tips";
//            [self.navigationController pushViewController:webViewController animated:YES];
//            
//            break;
//        }
//        case 5:
//        {
//            //            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//            //            webViewController.webURL = kContactURL;
//            //            webViewController.webTitle = @"Contact Support";
//            //            [self.navigationController pushViewController:webViewController animated:YES];
//            [self showSupportGridMenu];
//            break;
//        }
//        default:
//            break;
//    }
    
}

    
-(void) didSelectFirstGridMenuWithItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    
    switch (itemIndex) {
        case 0:
        {
            [self showSupportGridMenu];
            break;
        }
        case 1:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            
            
            if(self.mCarousel.currentItemIndex == 1){
                webViewController.webURL = kSpecsURL;
            }
            else if(self.mCarousel.currentItemIndex == 0){
                webViewController.webURL = kSpecsURL;
                
            }
            else if(self.mCarousel.currentItemIndex == 2){
                webViewController.webURL = kSpecsiPadURL;
                
            }
            else if(self.mCarousel.currentItemIndex == 3){
                webViewController.webURL = kSpecsiPodURL;
                
            }

            
            
          //  webViewController.webURL = kSpecsURL;
            webViewController.webTitle = @"Tips";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
           
        case 2:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
         //   webViewController.webURL = kCommunitiesURL;
            if(self.mCarousel.currentItemIndex == 1){
                webViewController.webURL = kCommunitiesiPhoneURL;
            }
            else if(self.mCarousel.currentItemIndex == 0){
                webViewController.webURL = kCommunitiesiMacURL;
                
            }
            else if(self.mCarousel.currentItemIndex == 2){
                webViewController.webURL = kCommunitiesiPadURL;
                
            }
            else if(self.mCarousel.currentItemIndex == 3){
                webViewController.webURL = kCommunitiesiPadURL;
                
            }

            
            webViewController.webTitle = @"Apple Support Communities";
            [self.navigationController pushViewController:webViewController animated:YES];

            break;
        }
            
        case 3:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            if(self.mCarousel.currentItemIndex == 1){
                webViewController.webURL = kManualsiPhoneURL;
            }
            else if(self.mCarousel.currentItemIndex == 0){
                webViewController.webURL = kManualsiMacURL;
                
            }
            else if(self.mCarousel.currentItemIndex == 2){
                webViewController.webURL = kManualsiPadURL;
                
            }
            else if(self.mCarousel.currentItemIndex == 3){
                webViewController.webURL = kManualsiPodURL;
                
            }
            webViewController.webTitle = @"Manuals";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case 4:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kVideoURL;
            webViewController.webTitle = @"Video Tutorials";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case 5:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kFAQURL;
            webViewController.webTitle = @"FAQs";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case 6:
        {
            //            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            //            webViewController.webURL = kContactURL;
            //            webViewController.webTitle = @"Contact Support";
            //            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        default:
            break;
    }
}

-(void) didSelectSecondGridMenuWithItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    switch (itemIndex) {
        case 0:
        {
        
                NSString *phNo = @"+1-800-275-2273";
                NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
                
                if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
                    [[UIApplication sharedApplication] openURL:phoneUrl];
                } else
                {
                    UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [calert show];
                }
                
            
            break;
        }
        case 1:
        {
            UINavigationController *navigationController  = (UINavigationController*) [self.storyboard instantiateViewControllerWithIdentifier:@"chatnavcontroller"];
                navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:navigationController animated:YES completion:nil];
            
            break;
        }
            default:
            break;
    
    }
}
    @end