//
//  ISSideMenuViewController.m
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ISSideMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "ISWebViewController.h"
#import "MapViewController.h"
//#import "CustomHeaderView.h"
//#import "EMLocationManager.h"
//#import "EMWeatherManager.h"
//#import "EMLoginManager.h"
//#import "EMDataManager.h"
//#import "EMUserProfileViewController.h"
//#import "EMSettingsViewController.h"
//#import "EMCommunicationManager.h"

#define kLocateURL @"https://locate.apple.com"
#define kCheckSupport @"https://selfsolve.apple.com/agreementWarrantyDynamic.do"
#define kCheckRepairStatus @"https://selfsolve.apple.com/repairstatus/main.do"


@interface ISSideMenuViewController ()

@property(strong, nonatomic)__block NSDictionary *addressResponse;
@property(strong, nonatomic)__block NSDictionary *weatherResponse;

@end

@implementation ISSideMenuViewController
{
    NSArray *titles;
    NSArray *images;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = YES;

    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = YES;
    self.tableView.scrollsToTop = NO;
    self.tableView.alwaysBounceVertical = YES;
    
    titles = @[@"My Dashboard", @"Check Repair Status", @"Locate Service Centers", @"Replacement Program", @"Check Service & Support", @"Send Diagnostics",@"My Messages",@"Send Feedback"];
    images = @[@"menu-home.png", @"menu-setup-repair.png", @"menu-locate.png", @"menu-replace.png", @"menu-create-cases.png", @"tech-specs.png", @"menu-my-messages.png", @"feedback-icon.png"];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // return;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    switch (indexPath.row) {

        case 0:
            navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"deviceDashboardController"]];
            [self.sideMenuViewController hideMenuViewController];
            break;

        case 1:{
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kCheckRepairStatus;
            webViewController.webTitle = @"Check Repair Status";
            webViewController.isFromFeedback = YES;
            
            navigationController.viewControllers = [NSArray arrayWithObject:webViewController];
            [self.sideMenuViewController hideMenuViewController];
            break;
        }
//        case 2:
//            
//            navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"caseListController"]];
//                       [self.sideMenuViewController hideMenuViewController];
//            break;
//        case 3:
//                navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"repairListController"]];
//                        [self.sideMenuViewController hideMenuViewController];
//        break;
//    
//        case 6:{
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])  {
//            
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = nil;
//            picker.allowsEditing = YES;
//            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            
//            [self presentViewController:picker animated:YES completion:NULL];
//                
//            }
//            else{
//                UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Camera facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [calert show];
//
//            }
//            break;
//        }

        case 2:
        {
//            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//            webViewController.webURL = kLocateURL;
//            webViewController.webTitle = @"Locate";
            MapViewController *mapViewController = (MapViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
            NSArray *storesArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MyData" ofType:@"plist"]];
            mapViewController.mapPinsArray=storesArray;
            mapViewController.isFromSideMenu = YES;
            
           
            
            navigationController.viewControllers = [NSArray arrayWithObject:mapViewController];
            [self.sideMenuViewController hideMenuViewController];

            break;
        }
        case 4:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kCheckSupport;
            webViewController.webTitle = @"Check Service and Support";
            webViewController.isFromFeedback = YES;
            
            navigationController.viewControllers = [NSArray arrayWithObject:webViewController];
            [self.sideMenuViewController hideMenuViewController];
            break;
        }

        case 5:
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"diags://"]];
            
            break;
        }
        case 6:
        {
                        break;
        }

            
        case 7:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = @"http://www.apple.com/feedback/";
            webViewController.webTitle = @"Send Feedback";
            webViewController.isFromFeedback = YES;
            
            navigationController.viewControllers = [NSArray arrayWithObject:webViewController];
            [self.sideMenuViewController hideMenuViewController];
            
            break;
        }

        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 138.0f;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *cellIdentifier = @"headerView";

    UITableViewCell* sectionHeaderCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    return sectionHeaderCell;
}


//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    static NSString *cellIdentifier = @"headerView";
//    
//    __block CustomHeaderView *sectionHeaderCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    sectionHeaderCell.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//
//    EMLoginManager *loginManager = [EMLoginManager sharedLoginManager];
//    if (loginManager.userProfile) {
//        sectionHeaderCell.profileNameLabel.text = loginManager.userProfile.userName;
//        
//        if (loginManager.userProfile.image)
//            sectionHeaderCell.profileImageView.image = loginManager.userProfile.image;
//
////        [sectionHeaderCell.profileImageView loadFromURLString:loginManager.userProfile.imageURL];
//
////        // Block variable to be assigned in block.
////        __block NSData *imageData;
////        dispatch_queue_t backgroundQueue  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
////        
////        // Dispatch a background thread for download
////        dispatch_async(backgroundQueue, ^(void) {
////            imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:loginManager.userProfile.imageURL]];
////            
////            UIImage *imageLoad;
////            imageLoad = [[UIImage alloc] initWithData:imageData];
////            
////            // Update UI on main thread
////            dispatch_async(dispatch_get_main_queue(), ^(void) {
////                sectionHeaderCell.profileImageView.image = imageLoad;
////            });
////        });
//
//    }
//
//    if (self.addressResponse) {
//        sectionHeaderCell.stateLabel.text = self.addressResponse[@"City"];
//        sectionHeaderCell.countryLabel.text = self.addressResponse[@"Country"];
//        
//    }
//    
//    if (self.weatherResponse) {
//        if ([[self.weatherResponse objectForKey:@"main"] objectForKey:@"temp"]) {
//            
//            CGFloat temperatureCelcius = ([[[self.weatherResponse objectForKey:@"main"] objectForKey:@"temp"] floatValue] - 272.15f);
//            
//            if ([[NSUserDefaults standardUserDefaults] boolForKey:kUnitsUserDefault]) {
//                sectionHeaderCell.temperatureLabel.text = [NSString stringWithFormat:@"%.0f°C", temperatureCelcius];
//            }
//            else {
//                sectionHeaderCell.temperatureLabel.text = [NSString stringWithFormat:@"%.0f°F", (temperatureCelcius * 1.8f)+32.0f];
//            }
//            
//            sectionHeaderCell.wheatherDetailLabel.text = [[[self.weatherResponse objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"main"];
//            
//            NSString *weatherIcon = [[[self.weatherResponse objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"icon"];
//            sectionHeaderCell.weatherImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", weatherIcon]];
//
//            [UIView animateWithDuration:2.0f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//                [sectionHeaderCell.weatherImageView setAlpha:1.0];
//            } completion:nil];
//
//            /*
//            // Block variable to be assigned in block.
//            __block NSData *imageData;
//            dispatch_queue_t backgroundQueue  = dispatch_queue_create("com.eclipsemedia.imageDownloadQueue", NULL);//dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            
//            // Dispatch a background thread for download
//            dispatch_async(backgroundQueue, ^(void) {
//                EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//                
//                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.png", [weatherManager weatherIconBaseURL], weatherIcon]]];
//                
//                UIImage *imageLoad;
//                imageLoad = [[UIImage alloc] initWithData:imageData];
//                
//                // Update UI on main thread
//                dispatch_async(dispatch_get_main_queue(), ^(void) {
//                                         [UIView transitionWithView:self.view
//                                                           duration:2.0f
//                                                            options:UIViewAnimationOptionTransitionCrossDissolve
//                                                         animations:^{
//                                                             sectionHeaderCell.weatherImageView.image = imageLoad;
//                                                         } completion:NULL];
//
//                });
//            });
//            */
//            /* Commented out due to performance issues
//            __weak EMSideMenuViewController *weakSelf = self;
//            EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//            NSURLRequest *imageDataRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.png", [weatherManager weatherIconBaseURL], weatherIcon]]];
//            [NSURLConnection sendAsynchronousRequest:imageDataRequest queue:[[EMCommunicationManager defaultManager] httpOperationQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//                
//                NSLog(@"called...");
//                        UIImage *imageLoad = [[UIImage alloc] initWithData:data];
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//                            sectionHeaderCell.weatherImageView.image = imageLoad;
//                            [sectionHeaderCell.weatherImageView setAlpha:0.0];
//                            [UIView animateWithDuration:2.0f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//                                [sectionHeaderCell.weatherImageView setAlpha:1.0];
//                            } completion:nil];
////                            [UIView transitionWithView:weakSelf.view
////                                              duration:2.0f
////                                               options:UIViewAnimationOptionTransitionCrossDissolve
////                                            animations:^{
////                                                sectionHeaderCell.weatherImageView.image = imageLoad;
////                                            } completion:NULL];
//                            
//                            
//                        });
//                
//            }];
//            */
//        }
//    }
//    
//    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePicSelected:)];
//    tapGesture.numberOfTapsRequired = 1;
//    [sectionHeaderCell addGestureRecognizer:tapGesture];
//    //[sectionHeaderCell setBackgroundColor:[UIColor redColor]];
//    return sectionHeaderCell;
//}

//- (void)profilePicSelected:(UITapGestureRecognizer*)tapGesture
//{
//    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EMUserProfileViewController *userProfileVC = (EMUserProfileViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"EMUserProfileViewController"];
//    [self presentViewController:userProfileVC animated:YES completion:^{
//        
//    }];
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController");
    
    [self.tableView reloadData];
    
//    /* Location details */
//    EMLocationManager *locationManager = [EMLocationManager sharedLocationManager];
//    [locationManager fetchActualUserAddressWithCompletionBlock:^(NSDictionary *response, NSError *error) {
//        self.addressResponse = response;
//        [self.tableView reloadData];
//    }];
//    
//    /* Weather details */
//    CLLocation *currentLocation = [locationManager actualUserLocation];
//    EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//    [self.view setBackgroundColor:[UIColor clearColor]];
//
//    [weatherManager fetchForecastOfLocationWithLatitude:[NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude]
//                                            andLogitude:[NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude]
//                                     andCompletionBlock:^(NSDictionary *response, NSError *error) {
//                                         self.weatherResponse = response;
//                                         [self.tableView reloadData];
//
//    }];
//    
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController");
}

- (IBAction)signOutBtnAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.navigationController.navigationBarHidden = NO;
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Logout"     // Event category (required)
//                                                          action:@"Eclipse"  // Event action (required)
//                                                           label:nil         // Event label
//                                                           value:nil] build]];    // Event value
//    
//    [EMActivityBar showWithStatus:@"Logging out.." forAction:@"Logout"];
//    RemoteOperation * operation = [[EMCommunicationManager defaultManager] operationForType:EMLogout];
//    
//    EMLoginManager *loginManager = [EMLoginManager sharedLoginManager];
//    [loginManager signOutSocialMedia];
//    
//    [[EMCommunicationManager defaultManager] addRemoteOperation:operation
//                                         usingCompletionHandler:^(RemoteOperation *operation, NSError *error) {
//                                             
//                                             if(operation != nil && error == nil)
//                                             {
//                                                 [EMActivityBar showSuccessWithStatus:@"Welcome to Eclipse!" forAction:@"Logout"];
//                                                 
//                                             }
//                                             
//                                             if (error)
//                                             {
//                                                 [EMActivityBar showErrorWithStatus:@"Logout Failed!" forAction:@"Logout"];
//                                             }
//                                             
//                                             loginManager.userProfile = [[EMUser alloc] init];
//                                             
//                                             EMDataManager *dataManager = [EMDataManager sharedInstance];
//                                             [dataManager resetDataManager];
//                                             
//                                             [self.navigationController popToRootViewControllerAnimated:YES];
//
//                                         }];
//     
}

- (IBAction)settingsBtnAction:(id)sender
{
//    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EMSettingsViewController *settingsVC = (EMSettingsViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
//    [self presentViewController:settingsVC animated:YES completion:^{
//        
//    }];
}

@end
