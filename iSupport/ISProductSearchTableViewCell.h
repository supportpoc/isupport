//
//  ISProductSearchTableViewCell.h
//  iSupport
//
//  Created by Asif on 04/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISProductSearchTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView* iconImgView;
@property (nonatomic, weak) IBOutlet UILabel* titleLbl;
@property (nonatomic, weak) IBOutlet UILabel* desc1Lbl;
@property (nonatomic, weak) IBOutlet UILabel* desc2Lbl;
@property (nonatomic, weak) IBOutlet UILabel* itemsDescLbl;

@end
