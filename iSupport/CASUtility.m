//
//  CASUtility.m
//  CAS
//
//  Created by Rakesh on 14/05/14.
//  Copyright (c) 2014 Apple. All rights reserved.
//

#import "CASUtility.h"
#import <CommonCrypto/CommonDigest.h>

@implementation CASUtility

static CASUtility *sharedUtility = nil;

+ (CASUtility *)sharedUtility
{
	if (!sharedUtility) {
		sharedUtility = [[super alloc] init];
        [sharedUtility setupProperties];
	}
    
	return sharedUtility;
}

- (void)setupProperties
{
    sharedUtility.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    sharedUtility.dateComponents = [[NSDateComponents alloc] init];
    sharedUtility.monthDayYearNumericFormatter = [[NSDateFormatter alloc] init];
    sharedUtility.monthDayYearNumericFormatter.dateFormat = @"M-d-yyyy";
    sharedUtility.yearMonthDayNumericFormatter = [[NSDateFormatter alloc] init];
    sharedUtility.yearMonthDayNumericFormatter.dateFormat = @"yyyy-MM-dd";
    sharedUtility.yearMonthDayTimestampNumericFormatter = [[NSDateFormatter alloc] init];
    sharedUtility.yearMonthDayTimestampNumericFormatter.dateFormat = @"yyyy-MM-dd hh:mm";
    sharedUtility.monthDayYearTimeNumericFormatter = [[NSDateFormatter alloc] init];
    sharedUtility.monthDayYearTimeNumericFormatter.dateFormat = @"M/d/yyyy h:mm a";
    sharedUtility.timeZoneDateFormatter = [[NSDateFormatter alloc] init];
    sharedUtility.timeZoneDateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    sharedUtility.timeZoneDateFormatterForWS = [[NSDateFormatter alloc] init];
    sharedUtility.timeZoneDateFormatterForWS.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    sharedUtility.timeZoneDateFormatterForWS.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    sharedUtility.dateTimeZoneFormatter = [[NSDateFormatter alloc] init];
    sharedUtility.dateTimeZoneFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    sharedUtility.dateTimeZoneFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SS'Z'";
    sharedUtility.monthDateYearFormatter = [[NSDateFormatter alloc]init];
    sharedUtility.monthDateYearFormatter.dateFormat = @"MMMM dd, yyyy";
    sharedUtility.hourMinuteFormatter = [[NSDateFormatter alloc]init];
    sharedUtility.hourMinuteFormatter.dateFormat = @"hh:mm a";
    
}

// Mon, Jun 1st, 2012
- (NSString *)ordinalStringFromDate:(NSDate *)date
{
    NSString *dayOfMonthWithSuffix;
    
    self.dateComponents = [self.calendar components: NSCalendarUnitDay fromDate:date];
    
    NSInteger dayOfMonth = [self.dateComponents day];
    
    if (dayOfMonth >= 11 && dayOfMonth <= 13) {
        dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldth", (long)dayOfMonth];
    } else {
        switch (dayOfMonth % 10) {
            case 1:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldst", (long)dayOfMonth];
                break;
                
            case 2:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldnd", (long)dayOfMonth];
                break;
                
            case 3:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldrd", (long)dayOfMonth];
                break;
                
            default:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldth", (long)dayOfMonth];
                break;
        }
    }
    
    NSDateFormatter *dayOfWeekMonthFormatter = [[NSDateFormatter alloc] init];
    dayOfWeekMonthFormatter.dateFormat = @"EEE, MMM";
    NSString *dayOfWeekMonth = [dayOfWeekMonthFormatter stringFromDate:date];
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    yearFormatter.dateFormat = @"yyyy";
    NSString *year = [yearFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@ %@, %@", dayOfWeekMonth, dayOfMonthWithSuffix, year];
}

- (NSString *)ordinalTimestampStringFromDate:(NSDate *)date
{
    NSString *dayOfMonthWithSuffix;
    
    self.dateComponents = [self.calendar components: NSCalendarUnitDay fromDate:date];
    
    NSInteger dayOfMonth = [self.dateComponents day];
    
    if (dayOfMonth >= 11 && dayOfMonth <= 13) {
        dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldth", (long)dayOfMonth];
    } else {
        switch (dayOfMonth % 10) {
            case 1:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldst", (long)dayOfMonth];
                break;
                
            case 2:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldnd", (long)dayOfMonth];
                break;
                
            case 3:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldrd", (long)dayOfMonth];
                break;
                
            default:
                dayOfMonthWithSuffix = [NSString stringWithFormat:@"%ldth", (long)dayOfMonth];
                break;
        }
    }
    
    NSDateFormatter *dayOfWeekMonthFormatter = [[NSDateFormatter alloc] init];
    dayOfWeekMonthFormatter.dateFormat = @"EEEE, MMMM";
    NSString *dayOfWeekMonth = [dayOfWeekMonthFormatter stringFromDate:date];
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    yearFormatter.dateFormat = @"yyyy";
    NSString *year = [yearFormatter stringFromDate:date];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateFormat = @"h:mma";
    NSString *time = [[timeFormatter stringFromDate:date] lowercaseString];
    
    return [NSString stringWithFormat:@"%@ %@, %@ %@", dayOfWeekMonth, dayOfMonthWithSuffix, year, time];
}
//10.45AM
- (NSString*)HourMinuteFromDate:(NSDate*)date{
    return [self.hourMinuteFormatter stringFromDate:date];
}
- (NSDate*)HourMinuteFromString:(NSString*)dateString{
    return [self.hourMinuteFormatter dateFromString:dateString];
}
//December 09,2013
- (NSString*)fullMonthdateYearFromDate:(NSDate*)date{
    return [self.monthDateYearFormatter stringFromDate:date];
}
- (NSDate*)fullMonthdateYearFromString:(NSString*)dateString{
    return [self.monthDateYearFormatter dateFromString:dateString];
}
// M-d-yyyy
- (NSString *)monthDayYearNumericStringFromNumericDate:(NSDate *)date
{
    return [self.monthDayYearNumericFormatter stringFromDate:date];
}

- (NSDate *)monthDayYearNumericDateFromNumericString:(NSString *)stringDate
{
    return [self.monthDayYearNumericFormatter dateFromString:stringDate];
}

// yyyy-MM-dd
- (NSDate *)yearMonthDayNumericDateFromString:(NSString *)dateString
{
    return [self.yearMonthDayNumericFormatter dateFromString:dateString];
}

- (NSString *)yearMonthDayNumericStringFromDate:(NSDate *)dateValue
{
    return [self.yearMonthDayNumericFormatter stringFromDate:dateValue];
}

// yyyy-MM-dd HH:mm:ss
- (NSDate *)yearMonthDayTimestampNumericDateFromNumericString:(NSString *)dateString
{
    return [self.yearMonthDayTimestampNumericFormatter dateFromString:dateString];
}

- (NSString *)yearMonthDayTimestampNumericStringFromNumericDate:(NSDate *)date
{
    return [self.yearMonthDayTimestampNumericFormatter stringFromDate:date];
}

// M-d-yyyy h:m a
- (NSDate *)monthDayYearTimeNumericDateFromNumericString:(NSString *)dateString
{
    return [self.monthDayYearTimeNumericFormatter dateFromString:dateString];
}

- (NSString *)monthDayYearTimeNumericStringFromNumericDate:(NSDate *)date
{
    return [self.monthDayYearTimeNumericFormatter stringFromDate:date];
}

// yyyy-MM-dd'T'HH:mm:ssZ
- (NSDate *)timeZoneDateFromString:(NSString *)dateString
{
    return [self.timeZoneDateFormatter dateFromString:dateString];
}

- (NSString *)timeZoneStringFromDate:(NSDate *)dateString
{
    return [self.timeZoneDateFormatter stringFromDate:dateString];
}

- (NSString *)removeAlphabetsFromDate:(NSString *)dateString
{
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"Z" withString:@" "];
    return dateString;
}

//yyyy-MM-dd'T'HH:mm:ss.SS'Z'
- (NSDate *)dateTimeZoneFromString:(NSString *)dateString
{
    return [self.dateTimeZoneFormatter dateFromString:dateString];
}


-(long)getYear:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    
    long year = [components year];
    return year;
}

-(NSString *)getDateOnlyFromDate:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    
    long year = [components year];
    long month = [components month];
    long day = [components day];
    
    return [NSString stringWithFormat:@"%ld/%ld/%ld",month,day,year];
}


- (NSString *)dateTimeZoneStringFromDate:(NSDate *)dateString
{
    return [self.dateTimeZoneFormatter stringFromDate:dateString];
}

- (NSString *)removeSpecialCharactersFromString:(NSString *)nameString
{
    nameString = [nameString stringByReplacingOccurrencesOfString:@"(" withString:@""];
    nameString = [nameString stringByReplacingOccurrencesOfString:@")" withString:@""];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    nameString = [nameString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return nameString;
}

- (BOOL)isSameDayWithDate:(NSDate *)date anotherDate:(NSDate *)anotherDate
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:anotherDate];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

- (NSMutableArray *)sortedArray:(NSArray *)tobeSortedArray withRespectToKey:(NSString *)sortKey
{
    NSSortDescriptor *frequencyDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey
                                                                        ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObjects:frequencyDescriptor, nil];
    return (NSMutableArray *)[tobeSortedArray sortedArrayUsingDescriptors:descriptors];
    
}

- (NSString *)replaceOccurenceOfAmpersandFromString:(NSString *)nameString
{
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    
    return nameString;
}
//reinstating special characters back
- (NSString *)reinstateSpecialCharactersInString:(NSString *)nameString
{
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    
    return nameString;
}

- (NSString *)removeWhiteSpaceFromString:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    return string;
}

- (NSString *)formatPhoneNumber:(NSString *)phoneNumber
{
    NSString *returnString = @"";
    
    if (phoneNumber.length == 0 || phoneNumber.length == 1) {
        returnString = phoneNumber;
    } else if ([[phoneNumber substringToIndex:1] isEqualToString:@"1"]) {
        phoneNumber = [self removeFormatFromString:phoneNumber];
        returnString = [self formatCountryCodePhoneNumber:phoneNumber];
    } else {
        phoneNumber = [self removeFormatFromString:phoneNumber];
        returnString = [self formatPrefixPhoneNumber:phoneNumber];
    }
    
    return returnString;
}

- (NSString *)removeFormatFromString:(NSString *)phoneNumber
{
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"+1" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return phoneNumber;
}

- (NSString *)formatCountryCodePhoneNumber:(NSString *)phoneNumber
{
    NSString *returnString;
    NSMutableString *mutablePhoneNumber = [phoneNumber mutableCopy];
    
    // If the string is not empty and begins with a 1, then delete the 1
    if (![mutablePhoneNumber isEqualToString:@""] && [[mutablePhoneNumber substringToIndex:1] isEqualToString:@"1"]) {
        [mutablePhoneNumber deleteCharactersInRange:(NSMakeRange(0, 1))];
    }
    
    NSInteger length = mutablePhoneNumber.length;
    
    if (length > 1 && length <= 2) {
        returnString = [NSString stringWithFormat:@"+1 (%@ )", mutablePhoneNumber];
        
    } else if (length == 3) {
        returnString = [NSString stringWithFormat:@"+1 (%@)", mutablePhoneNumber];
        
    } else if (length >= 4 && length <= 6) {
        returnString = [NSString stringWithFormat:@"+1 (%@) %@", [mutablePhoneNumber substringToIndex:3], [mutablePhoneNumber substringFromIndex:3]];
        
    } else if (length >= 7 && length <= 10) {
        returnString = [NSString stringWithFormat:@"+1 (%@) %@-%@", [mutablePhoneNumber substringToIndex:3], [mutablePhoneNumber substringWithRange:NSMakeRange(3, 3)], [mutablePhoneNumber substringFromIndex:6]];
        
    } else if (length >= 11) {
        returnString = [NSString stringWithFormat:@"1%@", mutablePhoneNumber];
    }
    
    return returnString;
}

- (NSString *)formatPrefixPhoneNumber:(NSString *)phoneNumber
{
    NSString *returnString;
    NSInteger length = phoneNumber.length;
    
    if (length > 1  && length <= 3) {
        returnString = [NSString stringWithFormat:@"%@", phoneNumber];
        
    } else if (length >= 4 && length <= 7) {
        returnString = [NSString stringWithFormat:@"%@-%@", [phoneNumber  substringToIndex:3], [phoneNumber substringFromIndex:3]];
        
    } else if (length >= 8 && length <= 10) {
        returnString = [NSString stringWithFormat:@"+1 (%@) %@-%@", [phoneNumber  substringToIndex:3], [phoneNumber substringWithRange:NSMakeRange(3, 3)], [phoneNumber substringFromIndex:6]];
        
    } else if (length >= 11) {
        returnString = phoneNumber;
    }
    
    return returnString;
}

- (NSMutableDictionary *)hashForMap:(NSMutableArray *)map byKey:(NSString *)key
{
    NSMutableDictionary *hash = [NSMutableDictionary dictionary];
    NSArray *mapCopy = [NSArray arrayWithArray:map];
    
    for (id entry in mapCopy) {
        [hash setObject:entry forKey:[entry valueForKey:key]];
    }
    
    return hash;
}
- (NSString *)formatTheCurrencyInLocalFormat:(NSNumber *)amount andCurrencyCode:(NSString*)currencyCode{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setCurrencyCode:currencyCode];
    [formatter setLocale:[NSLocale currentLocale]];
    NSString* currency = [formatter stringFromNumber:amount];
    return currency;
}

+ (NSString*)capitalizedTrimmedString:(NSString*)str {
    if (!str || [str isEqual:[NSNull null]]) {
        return @"";
    }
    NSString *cleanStr = [[str capitalizedString]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return cleanStr;
}

+(NSString*)dateInStringFromTodayNow:(int)daysAgoFromToday
{
    NSDate *now = [NSDate date];
    NSDate *argumentDaysAgo = [now dateByAddingTimeInterval:-daysAgoFromToday*24*60*60];
    NSString* dateString = [[CASUtility sharedUtility].timeZoneDateFormatterForWS stringFromDate:argumentDaysAgo];
    //making the time part explicitly 23:59:59, but this is incorrect since we are already setting the time zone of the formatter to GMT
    //might remove this later
    if (dateString) {
        dateString = [dateString stringByReplacingOccurrencesOfString:[[dateString componentsSeparatedByString:@"T"] objectAtIndex:1] withString: @"00:00:00"];
    }
    return dateString;
}

+(NSDate*)dateFromTodayNow:(int)daysAgoFromToday
{
    NSDate *now = [NSDate date];
    NSDate *argumentDaysAgo = [now dateByAddingTimeInterval:-daysAgoFromToday*24*60*60];
    return argumentDaysAgo;
}

+(NSArray *)distinct: (NSArray*) arrayOfObjects
{
    NSSet *set = [NSSet setWithArray:arrayOfObjects];
    return [set allObjects];
}

#pragma mark - color from hex color code
+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


+(BOOL)isYesterday:(NSDate *)notedate
{
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:notedate];
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init] ;
    [componentsToSubtract setDay:-1];
    
    NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:componentsToSubtract toDate:[NSDate date] options:0];
    
    NSDateComponents *yesterdaycomp = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:yesterday];
    if([yesterdaycomp day] == [otherDay day] &&
       [yesterdaycomp month] == [otherDay month] &&
       [yesterdaycomp year] == [otherDay year] &&
       [yesterdaycomp era] == [otherDay era]) {
        return TRUE;
    }
    else
    {
        return  false;
    }
}

+(BOOL)isToday:(NSDate *)notedate
{
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:notedate];
    
    NSDateComponents *today = [ [NSCalendar currentCalendar] components:( NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit |NSDayCalendarUnit) fromDate:[[NSDate alloc] init]];
    
    if([today day] == [otherDay day] &&
       [today month] == [otherDay month] &&
       [today year] == [otherDay year] &&
       [today era] == [otherDay era]) {
        return TRUE;
    }
    else
    {
        return  false;
    }
}

+(BOOL)is2to7daysAgo:(NSDate *)notedate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *today = [NSDate date];//[self Tempdatecreation:myStringDate];
    NSDateComponents *components = [gregorian components:NSDayCalendarUnit fromDate: [self ExtractonlyDate:notedate ]  toDate:[self ExtractonlyDate:today] options:0];
    NSInteger days =[components day];
    if (days >=  2 &&  days <= 7)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

+ (NSString*)GetDayOfWeekString:(NSDate *)indate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:indate];
    
    NSString *str;
    switch ([comps weekday])
    {
        case 2:
            str = @"Monday";
            break;
            
        case 3:
            str = @"Tuesday";
            break;
            
        case 4:
            str = @"Wednesday";
            break;
            
        case 5:
            str = @"Thursday";
            break;
            
        case 6:
            str = @"Friday";
            break;
            
        case 7:
            str = @"Saturday";
            break;
            
        case 1:
            str = @"Sunday";
            break;
        default: // should never coem here
            str = @"";
            break;
    }
    return str;
    
}

+(NSDate *)ExtractonlyDate:(NSDate  *)indate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateFromString = [dateFormatter stringFromDate:indate];
    return [dateFormatter dateFromString:dateFromString];
}

+(CGRect) getDynamicRectForText:(NSString *)text maxTextWidth:(float)maxTextWidth maxTextHeight:(float)maxTextHeight textFont:(UIFont*)textFont
{
    CGSize boundingSize = CGSizeMake(maxTextWidth-20, maxTextHeight);
    CGRect textRect = [text boundingRectWithSize:boundingSize
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName:textFont}
                                                          context:nil];
    
    return textRect;
}

@end

