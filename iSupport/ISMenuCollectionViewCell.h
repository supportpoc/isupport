//
//  ISMenuCollectionViewCell.h
//  iSupport_PROT_1
//
//  Created by Ankit on 03/06/14.
//  Copyright (c) 2014 Ankit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISMenuCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UILabel *menuOptionLabel;

@property (nonatomic, strong) NSString *imageName;

-(void)updateCell;
@end
