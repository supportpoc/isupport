//
//  CEProductTableViewCell.h
//  CustomerExperience
//
//  Created by Ila on 27/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"


@interface ProductTableViewCell : MGSwipeTableCell<MGSwipeTableCellDelegate>

@property(strong,nonatomic)IBOutlet UIImageView *productImage;
@property(strong,nonatomic)IBOutlet UILabel *productName;
@property(strong,nonatomic)IBOutlet UILabel *serialNo;
@property (strong, nonatomic) IBOutlet UILabel *tecnicalSupportExpDate;
@property (strong, nonatomic) IBOutlet UILabel *repairServiceExpDate;
@property (strong, nonatomic) IBOutlet UIImageView *supportStatusImage;
@property (strong, nonatomic) IBOutlet UIImageView *repairStatusImage;

@end
