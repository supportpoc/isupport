//
//  ISConstants.h
//  iSupport
//
//  Created by Varun on 16/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#ifndef iSupport_ISConstants_h
#define iSupport_ISConstants_h


#pragma mark - Manuals Url

#define kManualsURL @"http://support.apple.com/manuals/"
#define kManualsiPhoneURL @"http://support.apple.com/manuals/#iphone"
#define kManualsiPadURL @"http://support.apple.com/manuals/#ipad"
#define kManualsiMacURL @"http://support.apple.com/manuals/#macdesktops"
#define kManualsiPodURL @"http://support.apple.com/manuals/#ipod"


#pragma mark - Communities Url

#define kCommunitiesURL @"https://discussions.apple.com/welcome"
#define kCommunitiesiPadURL @"https://discussions.apple.com/community/ipad/using_ipad"
#define kCommunitiesiPhoneURL @"https://discussions.apple.com/community/iphone/using_iphone"
#define kCommunitiesiMacURL @"https://discussions.apple.com/community/desktop_computers/imac_intel"
#define kCommunitiesiPodURL @"https://discussions.apple.com/community/ipod/ipod_touch"



#pragma mark - Contact Url

#define kContactURL @"http://www.apple.com/support/contact/"


#pragma mark - Tips Url

#define kSpecsURL @"http://tips.apple.com/en-us/ios/iphone"
#define kSpecsiPadURL @"http://tips.apple.com/en-us/ios/ipad"
#define kSpecsiPodURL @"http://tips.apple.com/en-us/ios/ipod-touch"

#pragma mark - Locate Url

#define kLocateURL @"https://locate.apple.com"

#pragma mark - Video Url

#define kVideoURL @"http://support.apple.com/videos/"

#pragma mark - Contact Url

#define kContactURL @"http://www.apple.com/support/contact/"

#pragma mark - CheckSupport Url

#define kCheckSupport @"https://selfsolve.apple.com/agreementWarrantyDynamic.do"

#pragma mark - CheckRepairStatus Url

#define kCheckRepairStatus @"https://selfsolve.apple.com/repairstatus/main.do"

#pragma mark - FAQ Url

#define kFAQURL @"https://www.apple.com/in/support/products/faqs.html"

#endif
