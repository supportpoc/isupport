//
//  MyCasesView.h
//  iSupportTest
//
//  Created by Varun on 28/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCasesView : UIView

@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *serialNumber;
@property (strong, nonatomic) IBOutlet UILabel *dateCreated;
@property (strong, nonatomic) IBOutlet UILabel *caseStatus;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *probDescription;
@property (strong, nonatomic) IBOutlet UILabel *problem;


@end
