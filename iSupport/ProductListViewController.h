//
//  CEProductListViewController.h
//  CustomerExperience
//
//  Created by Ila on 27/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "RNGridMenu.h"

@interface ProductListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MGSwipeTableCellDelegate,RNGridMenuDelegate>
{
    IBOutlet UITableView *productTable;
}

@property(strong,nonatomic)ProductTableViewCell *productCell;
//@property(assign,nonatomic) bool *isPushed;
@property(assign,nonatomic) NSInteger *isPushed;


@end
