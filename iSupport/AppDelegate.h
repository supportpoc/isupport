//
//  AppDelegate.h
//  iSupport
//
//  Created by Asif on 27/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSArray *storesArray;

@end

