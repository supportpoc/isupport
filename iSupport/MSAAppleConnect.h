//
//  MSAAppleConnect.h
//  Tasks
//
//  Created by Rakesh Rawat on 7/9/13.
//  Copyright (c) 2013 Rakesh Rawat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACMAppleConnect.h"
#import <UIKit/UIKit.h>
#import "ISDashboardViewController.h"


typedef enum AppleConnectSequence {
    AppleConnectSequenceReady,
    AppleConnectSequenceAuthenticating,
    AppleConnectSequenceComplete
} AppleConnectSequence;


@interface MSAAppleConnect : NSObject <ACMAppleConnectDelegate, UIActionSheetDelegate>

@property (weak) id delegate;
@property (assign) AppleConnectSequence appleConnectSequence;
@property (strong) NSString *appleConnectTokenCache;
@property (assign) BOOL onlyNeedToken;
@property (assign) BOOL isAuthenticationScreenOn;
@property (assign) BOOL needsBeginOnboarding;
@property (assign) BOOL needsBeginSync;
@property (strong) NSString *userName;
@property (nonatomic, retain) ISDashboardViewController* contr;

+ (MSAAppleConnect *)sharedInstance;
+ (NSString *)getAppleID;
+ (void)setAppleID:(NSString *)appleID;

- (void)prepForConnect;
- (NSString *)appleConnectToken;
- (void)connect;
- (void)connectForOnboarding;
- (void)connectForSync;
- (void)executeAlertWithAuthenticationResponse:(id)response;
- (void)logout;

@end
