//
//  SearchViewController.m
//  iSupport
//
//  Created by Ankur on 28/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
// https://www.apple.com/retail/storelist/

#import "SearchViewController.h"
#import "SearchTableViewCell.h"
#import "MapViewController.h"

@interface SearchViewController ()<CLLocationManagerDelegate>{

    NSArray *storesArray;
    NSMutableArray *searchArray;
    BOOL isFiltered;
    int indexPathNo;
    CLLocationManager *locationManager;
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    storesArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MyData" ofType:@"plist"]];
    NSLog(@"searchArray = %@", storesArray);
    searchArray=[[NSMutableArray alloc]init];
    [self statLocationManager];
}

-(void)statLocationManager
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([locationManager respondsToSelector:@selector
         (requestWhenInUseAuthorization)]) {
    [locationManager requestWhenInUseAuthorization]; // Add This Line
    }
    [locationManager startUpdatingLocation];
    
    
    
}

// CLLocationManager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:
(NSArray *)locations
{
    NSLog(@"location info object=%@", [locations lastObject]);
    [locationManager stopUpdatingLocation];
    // CLLocationDistance meters = [newLocation distanceFromLocation:oldLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 5;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    {
        if(isFiltered)
            return [searchArray count];
        else
            return [storesArray count];
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dictTemp;
    if (!isFiltered) {
        
        dictTemp=[storesArray objectAtIndex:indexPath.row];
        cell.titleLabel.text=[dictTemp objectForKey:@"Title"];
        cell.descriptionLabel.text=[dictTemp objectForKey:@"Description"];
        [cell.globeBtn addTarget:nil action:@selector(globeBtnclicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.locationBtn addTarget:nil action:@selector(locationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

        
    }else{
    
        dictTemp=[searchArray objectAtIndex:indexPath.row];
        cell.titleLabel.text=[dictTemp objectForKey:@"Title"];
        cell.descriptionLabel.text=[dictTemp objectForKey:@"Description"];
        [cell.globeBtn addTarget:nil action:@selector(globeBtnclicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.locationBtn addTarget:nil action:@selector(locationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    }
       // Configure Cell
   // UILabel *label = (UILabel *)[cell.contentView viewWithTag:10];
   // [label setText:[NSString stringWithFormat:@"Row %i in Section %i", [indexPath row], [indexPath section]]];
    
    return cell;
}

- (IBAction)searchfieldTextchanged:(UITextField *)sender {
    NSLog(@"changing text %@",sender.text);
    
    if([sender.text length] == 0)
    {
        [searchArray removeAllObjects];
        
        isFiltered=NO;
    }
    else
    {
        isFiltered=YES;
        [searchArray removeAllObjects];
        for (NSString *dict in storesArray)
        {
            
            NSString *string=[dict valueForKey:@"Title"];
            
            NSRange stringrange=[string rangeOfString:sender.text options:NSCaseInsensitiveSearch];
            if (stringrange.location != NSNotFound) {
                [searchArray addObject:dict];
            }
        }
        
    }
    
    [searchTableView reloadData];

    
}


-(void)locationButtonClicked:(id)sender {
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *clickedButtonPath = [searchTableView indexPathForCell:clickedCell];
    indexPathNo=clickedButtonPath.row;
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
 
    
    if([segue.identifier isEqualToString:@"MapViewSegue"])
    {
        MapViewController *controller = (MapViewController *)segue.destinationViewController;
        if (isFiltered) {
            controller.mapPinsArray=searchArray;
        }
        else{
            controller.mapPinsArray=storesArray;
        }

        
        
    }else if([segue.identifier isEqualToString:@"CellMapViewSegue"]){
        
        MapViewController *controller = (MapViewController *)segue.destinationViewController;
        if (isFiltered) {
           
            NSArray *arrayMap=[[NSArray alloc]initWithObjects:[searchArray objectAtIndex:indexPathNo], nil];
            controller.mapPinsArray=arrayMap;
        }
        else{
            NSArray *arrayMap=[[NSArray alloc]initWithObjects:[storesArray objectAtIndex:indexPathNo], nil];
            controller.mapPinsArray=arrayMap;
            
        }
        
        
    }
    
}



-(void)globeBtnclicked{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.apple.com"]];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
