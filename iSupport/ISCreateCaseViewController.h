//
//  ISCreateCaseViewController.h
//  iSupport
//
//  Created by Asif on 18/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISCreateCaseViewController : UIViewController
{
    IBOutlet UITextView* _titleTxtView;
}

- (IBAction)cancelBtnAction:(id)sender;

@end
