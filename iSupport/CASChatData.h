//
//  CASChatData.h
//  CAS
//
//  Created by Sonali on 09/05/14.
//  Copyright (c) 2014 Sonali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CASChatData : NSObject

@property(nonatomic,retain)NSString *messageText;
@property(nonatomic,retain)NSString *messageTime;
@property(nonatomic,retain)NSString *bubbleImageName;
@property(nonatomic,retain)NSString *messageType;
@property(nonatomic,retain)NSString *messagesfrom;


@end
