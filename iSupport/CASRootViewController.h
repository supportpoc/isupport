//
//  ViewController.h
//  CAS
//
//  Created by Sonali on 09/05/14.
//  Copyright (c) 2014 Sonali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CASRootViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *chatNowBtn;

- (IBAction) startStopSpin :(id)sender;

@end
