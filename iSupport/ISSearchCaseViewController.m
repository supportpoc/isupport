//
//  ISSearchCaseViewController.m
//  iSupport
//
//  Created by Asif on 17/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import "ISSearchCaseViewController.h"

@interface ISSearchCaseViewController ()

@end

@implementation ISSearchCaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    _tableView.hidden = YES;
    [_textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneBtnAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)searchSolutionsBtnAction:(id)sender
{
    _tableView.hidden = NO;
    [_textView resignFirstResponder];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = nil;
    if (indexPath.row == 0)
    {
        cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CreateCaseCell0" forIndexPath:indexPath];
    }

    if (indexPath.row == 1)
    {
        cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CreateCaseCell1" forIndexPath:indexPath];
    }

    if (indexPath.row == 2)
    {
        cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CreateCaseCell2" forIndexPath:indexPath];
    }

    if (indexPath.row == 3)
    {
        cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CreateCaseCell3" forIndexPath:indexPath];
    }

    if (indexPath.row == 4)
    {
        cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CreateCaseCell4" forIndexPath:indexPath];
    }

    return cell;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _tableView.hidden = YES;
    
    return YES;
}

@end
