//
//  CERepairListViewController.m
//  CustomerExperience
//
//  Created by Ila on 27/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "RepairListViewController.h"
#import "ProductTableViewCell.h"
#import "RESideMenu.h"
#import "MGSwipeButton.h"
#import "ISSearchCaseViewController.h"

#define TEST_USE_MG_DELEGATE 1


@interface RepairListViewController ()


@property(nonatomic, strong) NSMutableArray *repairs;

@end

@implementation RepairListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.view.backgroundColor  = [UIColor colorWithRed:233.0/255 green:233.0/255.0 blue:233.0/255.0 alpha:1];
    //    productTable.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1];
    //   self.devices = [[NSMutableArray alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DashboardPlist" ofType:@"plist"];
    NSDictionary *myProducts = [NSDictionary dictionaryWithContentsOfFile:path];
    
    NSArray *repairList = [myProducts objectForKey:@"MyRepairs"];
    self.repairs = [[NSMutableArray alloc]initWithArray:repairList];
    
    
    
    
    /* Navigation Bar */
    
    self.navigationItem.title = @"Repair List";
    if(!self.isPushed){

    UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(showMenu:)];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    }
    
    UIImage *addImage = [UIImage imageNamed:@"menu-add-devices.png"];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithImage:addImage
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:nil];
    self.navigationItem.rightBarButtonItem = addButton;

    //    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //
}

- (void)showMenu:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //        return [self.details count];
    return [self.repairs count];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 141;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * repair =  [self.repairs objectAtIndex:indexPath.row];
    
    static NSString *popIdentifier = @"CEProductTableViewCell";
    self.repairCell = [tableView dequeueReusableCellWithIdentifier:popIdentifier];
    [self.repairCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if(self.repairCell == nil){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"RepairTableViewCell" owner:self options:nil];
        self.repairCell = [nib objectAtIndex:0];
    }
    self.repairCell.productImage.image = [UIImage imageNamed:[repair objectForKey:@"productImage"]];
    self.repairCell.productName.text = [repair objectForKey:@"productName"];
    self.repairCell.serialNumber.text = repair[@"serialNumber"];
    self.repairCell.dateCreated.text = [repair objectForKey:@"dateCreated"];
    self.repairCell.repairStatus.text = [repair objectForKey:@"repairStatus"];
    self.repairCell.repairStatus.layer.cornerRadius = 8;
    self.repairCell.repairStatus.layer.masksToBounds = YES;
    self.repairCell.problem.text = [repair objectForKey:@"problem"];
    self.repairCell.probDescription.text = [repair objectForKey:@"probDescription"];

    if([[repair objectForKey:@"repairStatus"] isEqualToString:@"Open" ]){
        self.repairCell.repairStatus.backgroundColor = [UIColor orangeColor];
    }
    else{
        self.repairCell.repairStatus.backgroundColor = [UIColor colorWithRed:75.0/255.0 green:186.0/255.0 blue:62.0/255.0 alpha:1.0];
    }
    //     self.repairCell.problem.text = [myCase objectForKey:@"problem"];
    //     self.repairCell.probDescription.text = [myCase objectForKey:@"probDescripton"];
    self.repairCell.delegate = self;
    self.repairCell.rightButtons = [self createRightButtons:1];
    return self.repairCell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showGrid];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

-(BOOL) swipeTableCell:(RepairTableViewCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        NSIndexPath * path = [repairTable indexPathForCell:cell];
        [self.repairs removeObjectAtIndex:path.row];
        [repairTable deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
    }
    
    return YES;
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    NSString* titles = @"Delete";
    UIColor * colors = [UIColor redColor];
    for (int i = 0; i < number; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles backgroundColor:colors callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (right).");
            return YES;
        }];
        [result addObject:button];
    }
    return result;
}

- (void)showGrid {
    NSInteger numberOfOptions = 4;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"create-a-case"] title:@"Create A Case"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"setup-a-repair"] title:@"Setup A Repair"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"take-in-service-circle"] title:@"Take In Service"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"extend-service-support-coverage"] title:@"Extend Support"],
                       
                       ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    
    // RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:161.0/255.0 blue:235.0/255.0 alpha:1.0 ];
    av.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    av.itemSize = CGSizeMake(150,150);
    av.blurLevel = 0.1;
    // av.itemFont = [UIFont boldSystemFontOfSize:10];
    
    //    av.bounces = NO;
    //[av showViewController:self sender:nil];
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex
{
    if (itemIndex == 0)
    {
        ISSearchCaseViewController *createCaseViewController = (ISSearchCaseViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"ISSearchCaseViewController"];
        
        [self presentViewController:createCaseViewController animated:YES completion:^{
            
        }];
    }
    
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
}



@end
