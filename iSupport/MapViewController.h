//
//  MapViewController.h
//  iSupport
//
//  Created by Ankur on 28/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController{

    IBOutlet MKMapView *mapView;
}
@property (nonatomic,strong) NSArray *mapPinsArray;
@property (nonatomic, assign) BOOL isFromSideMenu;

@end
