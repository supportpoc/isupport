//
//  MyDevicesView.m
//  iSupportTest
//
//  Created by Varun on 28/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import "MyDevicesView.h"

@implementation MyDevicesView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame {
    self = [[[NSBundle mainBundle] loadNibNamed:@"MyDevicesView" owner:self options:nil] objectAtIndex:0];
    self.frame = frame;
    return self;
}

@end
