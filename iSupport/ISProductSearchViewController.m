//
//  ISProductSearchViewController.m
//  iSupport
//
//  Created by Asif on 04/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import "ISProductSearchViewController.h"
#import "ISProductSearchTableViewCell.h"

@interface ISProductSearchViewController ()

@end

@implementation ISProductSearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    searchResults = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SearchResults" ofType:@"plist"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchResults count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 200;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"SearchResultCell";
    
    ISProductSearchTableViewCell* cell = (ISProductSearchTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSDictionary* dict = [searchResults objectAtIndex:indexPath.row];
    
    cell.iconImgView.image = [UIImage imageNamed:[dict valueForKey:@"icon"]];
    cell.titleLbl.text = [dict valueForKey:@"title"];
    cell.desc1Lbl.text = [dict valueForKey:@"description1"];
    cell.desc2Lbl.text = [dict valueForKey:@"description2"];
    cell.itemsDescLbl.text = [dict valueForKey:@"itemsDesc"];
    
    return cell;
}


@end
