//
//  ISDashboardViewController.h
//  iSupport
//
//  Created by Asif on 27/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "ISMenuCollectionView.h"
#import "MGSwipeTableCell.h"
#import "RNGridMenu.h"
#import "ISWebViewController.h"
#import "ISConstants.h"

@interface ISDashboardViewController : UIViewController<ISMenuCollectionViewDataSource, ISMenuCollectionViewDelegate,RNGridMenuDelegate>

@property (nonatomic, strong) NSMutableArray *carouselItems;

@property (nonatomic, strong) IBOutlet iCarousel *mCarousel;
@property (nonatomic, strong) IBOutlet ISMenuCollectionView *menuCollectionView;

- (IBAction)signInBtnAction:(id)sender;

@end
