//
//  CASUtility.h
//  CAS
//
//  Created by Rakesh on 14/05/14.
//  Copyright (c) 2014 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CASUtility : NSObject

@property (strong) NSCalendar *calendar;
@property (strong) NSDateComponents *dateComponents;
@property (strong) NSDateFormatter *monthDayYearNumericFormatter;
@property (strong) NSDateFormatter *yearMonthDayNumericFormatter;
@property (strong) NSDateFormatter *yearMonthDayTimestampNumericFormatter;
@property (strong) NSDateFormatter *monthDayYearTimeNumericFormatter;
@property (strong) NSDateFormatter *timeZoneDateFormatter;
@property (strong) NSDateFormatter *timeZoneDateFormatterForWS;
@property (strong) NSDateFormatter *dateTimeZoneFormatter;
@property (strong) NSDateFormatter *monthDateYearFormatter;
@property (strong) NSDateFormatter *hourMinuteFormatter;
+ (CASUtility *)sharedUtility;

// Mon, Jun 1st, 2012
- (NSString *)ordinalStringFromDate:(NSDate *)date;
// Monday, June 1st, 2012 8:00pm
- (NSString *)ordinalTimestampStringFromDate:(NSDate *)date;
// M-d-yyyy
- (NSDate *)monthDayYearNumericDateFromNumericString:(NSString *)stringDate;
- (NSString *)monthDayYearNumericStringFromNumericDate:(NSDate *)date;
// yyyy-MM-dd
- (NSDate *)yearMonthDayNumericDateFromString:(NSString *)dateString;
- (NSString *)yearMonthDayNumericStringFromDate:(NSDate *)dateValue;
// yyyy-MM-dd HH:mm:ss
- (NSDate *)yearMonthDayTimestampNumericDateFromNumericString:(NSString *)dateString;
- (NSString *)yearMonthDayTimestampNumericStringFromNumericDate:(NSDate *)date;
// M-d-yyyy h:mm a
- (NSDate *)monthDayYearTimeNumericDateFromNumericString:(NSString *)dateString;
- (NSString *)monthDayYearTimeNumericStringFromNumericDate:(NSDate *)date;
// yyyy-MM-dd'T'HH:mm:ssZ
- (NSDate *)timeZoneDateFromString:(NSString *)dateString;
- (NSString *)timeZoneStringFromDate:(NSDate *)dateString;
- (NSString *)removeAlphabetsFromDate:(NSString *)dateString;

// yyyy-MM-dd'T'HH:mm:ss.SS'Z'
- (NSDate *)dateTimeZoneFromString:(NSString *)dateString;
- (NSString *)dateTimeZoneStringFromDate:(NSDate *)dateString;

//December 09,2013
- (NSDate*)fullMonthdateYearFromString:(NSString*)dateString;
- (NSString*)fullMonthdateYearFromDate:(NSDate*)dateString;
//10.45AM
- (NSString*)HourMinuteFromDate:(NSDate*)dateString;
- (NSDate*)HourMinuteFromString:(NSString*)dateString;
- (BOOL)isSameDayWithDate:(NSDate *)date anotherDate:(NSDate *)anotherDate;

- (NSMutableArray *)sortedArray:(NSArray *)tobeSortedArray withRespectToKey:(NSString *)sortKey;

- (NSString *)removeSpecialCharactersFromString:(NSString *)nameString;
- (NSString *)replaceOccurenceOfAmpersandFromString:(NSString *)nameString;
- (NSString *)removeWhiteSpaceFromString:(NSString *)string;

- (NSString *)formatPhoneNumber:(NSString *)phoneNumber;
- (NSString *)removeFormatFromString:(NSString *)phoneNumber;
- (NSString *)formatCountryCodePhoneNumber:(NSString *)phoneNumber;
- (NSString *)formatPrefixPhoneNumber:(NSString *)phoneNumber;

- (NSMutableDictionary *)hashForMap:(NSMutableArray *)map byKey:(NSString *)key;
- (NSString *)formatTheCurrencyInLocalFormat:(NSNumber *)amount andCurrencyCode:(NSString*)currencyCode;
-(long)getYear:(NSDate*)date;

// 02/01/2014
-(NSString *)getDateOnlyFromDate:(NSDate*)date;

//reinstating special characters back
- (NSString *)reinstateSpecialCharactersInString:(NSString *)nameString;

+ (NSString*)capitalizedTrimmedString:(NSString*)str;
+(NSString*)dateInStringFromTodayNow:(int)daysAgoFromToday;
+(NSDate*)dateFromTodayNow:(int)daysAgoFromToday;
+(NSArray *)distinct: (NSArray*) arrayOfObjects;
//color from hex color code
+(UIColor *)colorWithHexString:(NSString*)hex ;


// returns True if the given date is Todays date
+(BOOL)isYesterday:(NSDate *)notedate;

// returns True if the given date is yesterdays date
+(BOOL)isToday:(NSDate *)notedate;

// returns True if the given date is between today and last 7 days
+(BOOL)is2to7daysAgo:(NSDate *)notedate;

+ (NSString*) GetDayOfWeekString:(NSDate *)indate;
+(CGRect) getDynamicRectForText:(NSString *)text maxTextWidth:(float)maxTextWidth maxTextHeight:(float)maxTextHeight textFont:(UIFont*)textFont;

@end
