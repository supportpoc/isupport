//
//  ISSearchCaseViewController.h
//  iSupport
//
//  Created by Asif on 17/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSearchCaseViewController : UIViewController
{
    IBOutlet UITableView* _tableView;
    IBOutlet UITextView* _textView;
}

- (IBAction)doneBtnAction:(id)sender;
- (IBAction)searchSolutionsBtnAction:(id)sender;

@end
