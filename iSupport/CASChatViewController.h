//
//  CASChatViewController.h
//  CAS
//
//  Created by Sonali on 09/05/14.
//  Copyright (c) 2014 Sonali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface CASChatViewController : UIViewController<HPGrowingTextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *chatCollectionView;

@end
