//
//  ISWebViewController.h
//  iSupport
//
//  Created by Upakul on 04/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ISWebViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *webURL;
@property (nonatomic, strong) NSString *webTitle;
@property (nonatomic, assign) BOOL isFromFeedback;
@property (nonatomic, assign) BOOL isFromLocate;

@end
