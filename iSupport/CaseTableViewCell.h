//
//  CareTableViewCell.h
//  iSupport
//
//  Created by Varun on 03/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"


@interface CaseTableViewCell : MGSwipeTableCell<MGSwipeTableCellDelegate>

@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *serialNumber;
@property (strong, nonatomic) IBOutlet UILabel *dateCreated;
@property (strong, nonatomic) IBOutlet UILabel *caseStatus;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *problem;
@property (strong, nonatomic) IBOutlet UILabel *probDescription;

@end
