//
//  CaseListViewController.h
//  iSupport
//
//  Created by Varun on 03/12/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "RNGridMenu.h"


@interface CaseListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MGSwipeTableCellDelegate,RNGridMenuDelegate>
{
    IBOutlet UITableView *caseTable;
}

@property(strong,nonatomic) CaseTableViewCell *caseCell;
@property(assign,nonatomic) NSInteger *isPushed;


@end
