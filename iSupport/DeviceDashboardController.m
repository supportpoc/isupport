//
//  ViewController.m
//  iSupportTest
//
//  Created by Varun on 27/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import "DeviceDashboardController.h"
#import "MyDevicesView.h"
#import "MyCasesView.h"
#import "MyRepairsView.h"
#import "RESideMenu.h"
#import "ProductListViewController.h"
#import "CaseListViewController.h"
#import "RepairListViewController.h"
#import "ISWebViewController.h"
#import "ISDashboardViewController.h"



@interface DeviceDashboardController ()

@end

@implementation DeviceDashboardController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.title = @"Dashboard";
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DashboardPlist" ofType:@"plist"];
     self.myProducts = [NSDictionary dictionaryWithContentsOfFile:path];
    

    // self.menuCollectionView.pageControl.numberOfPages = 3;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - TableView DataSource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    return [self.myProducts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"";
    
    switch ([indexPath row]) {
        case 0:{
            cellIdentifier = @"MyDeviceCellIdentfier";
            self.myDeviceCell  = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

            if (self.myDeviceCell == nil) {
                self.myDeviceCell = [[MyDevicesCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellIdentifier];
               

            }
            self.myDeviceCell.productNumber.text = @"1";
            NSArray* myDevices =  [self.myProducts objectForKey:@"MyDevices"];
            self.myDeviceCell.totalDevices.text = [NSString stringWithFormat:@"%lu",[myDevices count] ];

            for (int i = 0; i < [myDevices count]; i++) {
                CGRect frame;
                frame.origin.x = self.myDeviceCell.myDevicesScrollView.frame.size.width * i;
                frame.origin.y = 0;
                frame.size = self.myDeviceCell.myDevicesScrollView.frame.size;
                MyDevicesView *subview = [[MyDevicesView alloc] initWithFrame:frame];
                NSDictionary *myDevice = [myDevices objectAtIndex:i];
                
                subview.productName.text = [myDevice objectForKey:@"productName"];
                subview.serialNumber.text = [myDevice objectForKey:@"serialNumber"];

                subview.tecnicalSupportExpDate.text = [NSString stringWithFormat:@"Technical Support: %@",[myDevice objectForKey:@"tecnicalSupportExpDate"]];
                subview.repairServiceExpDate.text = [myDevice objectForKey:@"repairServiceExpDate"];
                [subview.productImage  setImage:[UIImage imageNamed:[myDevice objectForKey:@"productImage"]]];
                if([[myDevice objectForKey:@"tecnicalSupportExpDate"] isEqualToString:@"Expired"]){
                    [subview.supportStatusImage  setImage:[UIImage imageNamed:@"invalid-icon"]];
                }
                else{
                    [subview.supportStatusImage  setImage:[UIImage imageNamed:@"valid-icon"]];
                }
                
                if([[myDevice objectForKey:@"repairServiceExpDate"] isEqualToString:@"Expired"]){
                    [subview.repairStatusImage  setImage:[UIImage imageNamed:@"invalid-icon"]];
                }
                else{
                    [subview.repairStatusImage  setImage:[UIImage imageNamed:@"valid-icon.png"]];
                }
                
                self.myDeviceCell.myDevicesScrollView.tag = [indexPath row];
                [self.myDeviceCell.myDevicesScrollView addSubview:subview];
            }

            self.myDeviceCell.myDevicesScrollView.contentSize = CGSizeMake(self.myDeviceCell.myDevicesScrollView.frame.size.width * [myDevices count] ,self.myDeviceCell.myDevicesScrollView.frame.size.height);
        [self.myDeviceCell.myDevicesScrollView setUserInteractionEnabled:NO];
            [self.myDeviceCell.contentView addGestureRecognizer:self.myDeviceCell.myDevicesScrollView.panGestureRecognizer];
            return self.myDeviceCell;
            break;
        }
        case 1:{
            
            cellIdentifier = @"MyCaseCellIdentfier";
            self.myCaseCell  = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (self.myCaseCell == nil) {
                self.myCaseCell = [[MyCasesCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellIdentifier];
               // cell.productName.text = @"iPhone";
                
            }
            self.myCaseCell.productNumber.text = @"1";

            NSArray* myCases =  [self.myProducts objectForKey:@"MyCases"];
            self.myCaseCell.totalDevices.text = [NSString stringWithFormat:@"%lu",[myCases count] ];
            for (int i = 0; i < [myCases count]; i++) {
                CGRect frame;
                frame.origin.x = self.myCaseCell.myCasesScrollView.frame.size.width * i;
                frame.origin.y = 0;
                frame.size = self.myCaseCell.myCasesScrollView.frame.size;
                MyCasesView *subview = [[MyCasesView alloc] initWithFrame:frame];
                NSDictionary *myCase = [myCases objectAtIndex:i];
                
                subview.productName.text = [myCase objectForKey:@"productName"];
                subview.serialNumber.text = [myCase objectForKey:@"serialNumber"];
                subview.dateCreated.text = [myCase objectForKey:@"dateCreated"];
                subview.caseStatus.text = [myCase objectForKey:@"caseStatus"];
                subview.caseStatus.layer.cornerRadius = 8;
                subview.caseStatus.layer.masksToBounds = YES;
                subview.problem.text = [myCase objectForKey:@"problem"];

                if([[myCase objectForKey:@"caseStatus"] isEqualToString:@"Open" ]){
                    subview.caseStatus.backgroundColor = [UIColor orangeColor];
                }
                else{
                    subview.caseStatus.backgroundColor = [UIColor colorWithRed:75.0/255.0 green:186.0/255.0 blue:62.0/255.0 alpha:1.0];
                }
                [subview.productImage  setImage:[UIImage imageNamed:[myCase objectForKey:@"productImage"]]];
//                subview.problem.text = [myCase objectForKey:@"problem"];
                [self.myCaseCell.myCasesScrollView addSubview:subview];
            }
            self.myCaseCell.myCasesScrollView.tag = [indexPath row];
            self.myCaseCell.myCasesScrollView.contentSize = CGSizeMake(self.myCaseCell.myCasesScrollView.frame.size.width * [myCases count], self.myCaseCell.myCasesScrollView.frame.size.height);
          [self.myCaseCell.myCasesScrollView setUserInteractionEnabled:NO];
        [self.myCaseCell.contentView addGestureRecognizer:self.myCaseCell.myCasesScrollView.panGestureRecognizer];
            return self.myCaseCell;
            break;
        }
        case 2:{
            cellIdentifier = @"MyRepairCellIdentfier";
            self.myRepairCell  = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
           // MyRepairsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

            if (self.myRepairCell == nil) {
                self.myRepairCell = [[MyRepairsCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellIdentifier];
              //  cell.productName.text = @"iPhone";
                
            }
            self.myRepairCell.productNumber.text = @"1";

            NSArray* myRepairs =  [self.myProducts objectForKey:@"MyRepairs"];
            self.myRepairCell.totalDevices.text = [NSString stringWithFormat:@"%lu",[myRepairs count] ];
            for (int i = 0; i < [myRepairs count]; i++) {
                CGRect frame;
                frame.origin.x = self.myRepairCell.myRepairsScrollView.frame.size.width * i;
                frame.origin.y = 0;
                frame.size = self.myRepairCell.myRepairsScrollView.frame.size;
                MyRepairsView *subview = [[MyRepairsView alloc] initWithFrame:frame];
                NSDictionary *myRepair = [myRepairs objectAtIndex:i];
                
                subview.productName.text = [myRepair objectForKey:@"productName"];
                subview.serialNumber.text = [myRepair objectForKey:@"serialNumber"];
                subview.dateCreated.text = [myRepair objectForKey:@"dateCreated"];
                subview.caseStatus.text = [myRepair objectForKey:@"repairStatus"];
                subview.caseStatus.layer.cornerRadius = 8;
                subview.caseStatus.layer.masksToBounds = YES;
                subview.problem.text = [myRepair objectForKey:@"problem"];

                if([[myRepair objectForKey:@"repairStatus"] isEqualToString:@"Open" ]){
                    subview.caseStatus.backgroundColor = [UIColor orangeColor];
                }
                
                else{
                    subview.caseStatus.backgroundColor = [UIColor colorWithRed:75.0/255.0 green:186.0/255.0 blue:62.0/255.0 alpha:1.0];
                }
            
                [subview.productImage  setImage:[UIImage imageNamed:[myRepair objectForKey:@"productImage"]]];
                self.myRepairCell.myRepairsScrollView.tag = [indexPath row];
                [self.myRepairCell.myRepairsScrollView addSubview:subview];
            }
            
            self.myRepairCell.myRepairsScrollView.contentSize = CGSizeMake(self.myRepairCell.myRepairsScrollView.frame.size.width * [myRepairs count], self.myRepairCell.myRepairsScrollView.frame.size.height);
           [self.myRepairCell.myRepairsScrollView setUserInteractionEnabled:NO];
        [self.myRepairCell.contentView addGestureRecognizer:self.myRepairCell.myRepairsScrollView.panGestureRecognizer];
            return self.myRepairCell;
            break;
        }
        default:{
            cellIdentifier = @"MyRepairCellIdentfier";
            MyCasesCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [[MyCasesCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellIdentifier];
            //    cell.productName.text = @"iPhone";
                
            }
            return cell;
            break;
        }
    }
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    switch ([indexPath row]) {
        case 0:
        {
        ProductListViewController *rootVC = (ProductListViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"productListController"];
            rootVC.isPushed =  1;
            [self.navigationController pushViewController:rootVC animated:YES];
            break;
        }
        case 1:{
            CaseListViewController *rootVC = (CaseListViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"caseListController"];
            rootVC.isPushed =  1;

            [self.navigationController pushViewController:rootVC animated:YES];
            break;

        }
        case 2:{
            RepairListViewController *rootVC = (RepairListViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"repairListController"];
            rootVC.isPushed =  1;

            [self.navigationController pushViewController:rootVC animated:YES];
            break;
        }
            
        default:
            break;
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    if(scrollView.tag == 0){
        self.myDeviceCell.productNumber.text = [NSString stringWithFormat:@"%d", page+1];
    }
    else if (scrollView.tag == 1){
        self.myCaseCell.productNumber.text = [NSString stringWithFormat:@"%d", page+1];

    }
    else{
        self.myRepairCell.productNumber.text = [NSString stringWithFormat:@"%d", page+1];
  
    }
}

#pragma mark - menu options datasource/delegate

-(NSArray*) menuOptiosArrayForCollectionView:(ISMenuCollectionView *)menuCollectionView{
    return [NSArray arrayWithObjects:
            @"Contact Apple Support",
           
            @"Tips",

            @"Apple Support Communities",
        
            @"Manuals",

            @"Videos Tutorials"

            ,@"FAQs", nil];
    
   // @"Downloads"
}
-(NSArray*) menuIconsArrayForCollectionView:(ISMenuCollectionView *)menuCollectionView{
    return [NSArray arrayWithObjects:
            @"contact-apple-support.png",
           
            @"tech-specs.png",

            @"apple-support-communities.png",
            
            

            @"manuals.png.png",

            @"video-tutorials.png",

            @"faqs-icon.png",nil];
//    @"download-icon.png"
}

-(void) menuCollectionView:(ISMenuCollectionView *)menuCollectionView selectedItemAtIndexpath:(NSIndexPath *)indexpath{
    
    NSLog(@"selected item at index %ld", (long)indexpath.row);
    switch (indexpath.row) {
        
        case 0:
        {
            [self showSupportGridMenu];
            break;
        }
        case 1:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kSpecsURL;
            webViewController.webTitle = @"Tips";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case 2:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kCommunitiesURL;
            webViewController.webTitle = @"Apple Support Communities";
            [self.navigationController pushViewController:webViewController animated:YES];


            break;
        }
        case 3:
        {
            
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            
                webViewController.webURL = kManualsURL;
                       webViewController.webTitle = @"Manuals";
            [self.navigationController pushViewController:webViewController animated:YES];

            break;
        }
        case 4:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kVideoURL;
            webViewController.webTitle = @"Video Tutorials";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;        }
        case 5:
        {
            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            webViewController.webURL = kFAQURL;
            webViewController.webTitle = @"FAQs";
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
            
        }
        case 6:
        {
            break;
        }
            
        case 9:
        {
            
            break;
        }
            
        default:
            break;
    }
}


-(void) showSupportGridMenu {
    NSInteger numberOfOptions = 4;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"schedule-a-call.png"] title:@"Call"],
                       [ [RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat-with-executive.png"] title:@"Chat"],
                       [ [RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"menu-my-messages"] title:@"Mail"],
                       [ [RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"contact-a-mobile-carrier.png"] title:@"Call Back"],
                       ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.tag = [NSNumber numberWithInteger:2];
    
    av.delegate = self;
    av.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
      av.itemSize = CGSizeMake(150,100);
    av.blurLevel = 0.1;
    av.itemFont = [UIFont boldSystemFontOfSize:12];
    
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
    
    
}


#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
    switch (itemIndex) {
        case 0:
        {
                NSString *phNo = @"+1-800-275-2273";
                NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
                
                if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
                    [[UIApplication sharedApplication] openURL:phoneUrl];
                } else
                {
                    UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [calert show];
            
                
            }
            break;
        }
        case 1:
        {
            
            
                UINavigationController *navigationController  = (UINavigationController*) [self.storyboard instantiateViewControllerWithIdentifier:@"chatnavcontroller"];
                navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:navigationController animated:YES completion:nil];
            break;
        }
        case 2:
        {
           // MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
           // [comp setMailComposeDelegate:nil];
           // if([MFMailComposeViewController canSendMail]) {
           //     [comp setToRecipients:[NSArray arrayWithObjects:@"imstkgp@gnail.com", nil]];
           //     [comp setSubject:@"From my app"];
           //     [comp setMessageBody:@"Hello bro" isHTML:NO];
           //     [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
           //     [self presentViewController:comp animated:YES completion:nil];
           // }
            
            
            break;
        }

        default:
            break;
    }


}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if(error) {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"" otherButtonTitles:nil, nil];
        [alrt show];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)showMainMenuBtnAction:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

@end
