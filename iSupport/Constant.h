//
//  Constant.h
//  CAS
//
//  Created by Sonali on 12/05/14.
//  Copyright (c) 2014 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Constant : NSObject

#define headerViewfont [UIFont fontWithName:@"HelveticaNeue" size:12.0]


@end
