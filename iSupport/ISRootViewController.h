//
//  ISRootViewController.h
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

/**
  its is initial View controller
 */
@interface ISRootViewController : RESideMenu

@end
