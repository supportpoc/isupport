//
//  MSAAppleConnect.m
//  Tasks
//
//  Created by Rakesh Rawat on 7/9/13.
//  Copyright (c) 2013 Rakesh Rawat. All rights reserved.
//

#import "MSAAppleConnect.h"
#import "AppDelegate.h"

static NSString *const kACMSampleAppIDKey = @"4d768817bef7ba7b308f5fd66f21914ca963c8c73c29be4cb476f5f2495795c5";


static MSAAppleConnect *sharedAppleConnect = nil;

@implementation MSAAppleConnect

+ (MSAAppleConnect *)sharedInstance
{
	if (!sharedAppleConnect) {
		sharedAppleConnect = [[super alloc] init];
        sharedAppleConnect.onlyNeedToken = NO;
        sharedAppleConnect.needsBeginOnboarding = NO;
	}
    
	return sharedAppleConnect;
}

+ (NSString *)getAppleID
{
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    return [userDefaults objectForKey:kTBAAppleConnectID];
    return nil;
}

+ (void)setAppleID:(NSString *)appleID
{
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setObject:appleID forKey:kTBAAppleConnectID];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)prepForConnect
{
    self.appleConnectSequence = AppleConnectSequenceReady;
    self.isAuthenticationScreenOn = NO;
    self.appleConnectTokenCache = @"";
}

- (NSString *)appleConnectToken
{
    self.onlyNeedToken = YES;
    [self prepForConnect];
    
    [self connect];
    
    while ([self.appleConnectTokenCache isEqualToString:@""]) {
        
    }
    
    return self.appleConnectTokenCache;
}

- (void)connectForOnboarding
{
//    TBALog(@"APPLE_CONNECT: Inside Connect For Onboarding");
//    
//    self.needsBeginOnboarding = YES;
//    [self prepForConnect];
//    [self connect];
}

- (void)connectForSync{
    

//{
//    TBALog(@"APPLE_CONNECT: Inside Connect For Sync");
//    NetworkStatus netStatus = [[[MSAAppDelegate appDelegate] internetReach] currentReachabilityStatus];
//    if (netStatus != NotReachable) {
//        self.needsBeginSync = YES;
//        [self prepForConnect];
//        [self connect];
//    } else {
//        TBALog(@"APPLE_CONNECT: Network not rechable");
//    }
}

- (void)connect
{
   // TBALog(@"APPLE_CONNECT: Inside Connect");
    
//    if (self.appleConnectSequence != AppleConnectSequenceAuthenticating)
    {
        self.appleConnectSequence = AppleConnectSequenceAuthenticating;
        id<ACMAuthenticationRequest> aCMAuthenticationRequest = [ACMRequest authenticationRequest];
        aCMAuthenticationRequest.realm = @"APPLECONNECT-UAT.APPLE.COM"; //[TBAUtility getAppleConnectRealm];
        aCMAuthenticationRequest.appID = [NSNumber numberWithInt:1157];
        aCMAuthenticationRequest.appIDKey = kACMSampleAppIDKey;//[TBAUtility getAppleConnectAppIDKey];
//        aCMAuthenticationRequest.disableSingleSignOn = NO;
        
        ACMAppleConnect *sharedAppleConnect = [ACMAppleConnect sharedInstance];
        sharedAppleConnect.logLevel = kACMLogLevelVerbose;
        sharedAppleConnect.delegate = self;
        
//        if (!self.isAuthenticationScreenOn)
        {
            self.isAuthenticationScreenOn = YES;
            [sharedAppleConnect authenticate:aCMAuthenticationRequest];
        }
    }
}

#pragma mark - AppleConnect Delegate methods

- (void)appleConnectDidHideSignInDialog:(ACMAppleConnect *)appleConnect
{
    
}

- (UIViewController *)appleConnectParentViewController:(ACMAppleConnect *)iAppleConnect
{
//    TBALog(@"APPLE_CONNECT: Returning ACM Parent View Controller");
//    
//	return [((AppDelegate*)[UIApplication sharedApplication]).window rootViewController];
    return self.contr;
}

- (ACMContentStyle)appleConnectSignInDialogContentStyle:(ACMAppleConnect *)appleConnect
{
    return kACMContentStyleDark;
}

- (void)appleConnectWillShowSignInDialog:(ACMAppleConnect *)appleConnect
{
    
}

- (void)appleConnectDidShowSignInDialog:(ACMAppleConnect *)appleConnect
{
    
}

- (UIView *)appleConnectManagerApprovalDialogSummaryView:(ACMAppleConnect *)appleConnect
{
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame =  CGRectMake(0, 100, 50, 50);
    [btn setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];

    btn.titleLabel.text = @"Cancel";
    
    return btn;
}

- (void)appleConnect:(ACMAppleConnect *)appleConnect authenticationDidEndWithResponse:(id<ACMAuthenticationResponse>)response
{
//    self.userName = response.userName;
//    
//    self.isAuthenticationScreenOn = NO;
//	if (!response.error) {
//        self.appleConnectTokenCache = response.token;
//        [MSAAppleConnect setAppleID:response.userName];
//        
//        if (self.onlyNeedToken) {
//            self.appleConnectSequence = AppleConnectSequenceComplete;
//            self.onlyNeedToken = NO;
//            
//        } else  if (self.needsBeginOnboarding) {
//            self.appleConnectSequence = AppleConnectSequenceComplete;
//            [[TBADataController sharedInstance] beginOnboardingProcess];
//            self.needsBeginOnboarding = NO;
//            
//        } else  if (self.needsBeginSync) {
//            self.appleConnectSequence = AppleConnectSequenceComplete;
//            //[[TBADataController sharedInstance] beginSyncSequence];
//            self.needsBeginSync = NO;
//            
//        } else {
//            self.appleConnectSequence = AppleConnectSequenceComplete;
//            [[MSAAppDelegate appDelegate] performOnboardingCheck];
//        }
//        
//	} else {
//        [self executeAlertWithAuthenticationResponse:response];
//    }
}

- (void)appleConnect:(ACMAppleConnect *)appleConnect ticketVerificationDidEndWithResponse:(id<ACMTicketVerificationResponse>)response
{
}

- (void)executeAlertWithAuthenticationResponse:(id)response
{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                    message:[NSString
//                                                             stringWithFormat:@"%d: %@",
//                                                             [[response error] code],
//                                                             [[response error] localizedDescription]]
//                                                   delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
}

- (void)logout
{
//    TBALog(@"APPLE_CONNECT: Logging Out");
//    ACMAppleConnect *appleConnect = [ACMAppleConnect sharedInstance];
//    [appleConnect logoutUser:self.userName inRealm:[TBAUtility getAppleConnectRealm]];
}

@end