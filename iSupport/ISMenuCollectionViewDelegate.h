//
//  ISMenuCollectionViewDelegate.h
//  iSupport_PROT_1
//
//  Created by Ankit on 04/06/14.
//  Copyright (c) 2014 Ankit. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ISMenuCollectionView;
@protocol ISMenuCollectionViewDelegate <NSObject>
-(void) menuCollectionView:(ISMenuCollectionView*) menuCollectionView selectedItemAtIndexpath:(NSIndexPath*) indexpath;
@end
