//
//  ChatCollectionHeaderView.h
//  CAS
//
//  Created by Sonali on 09/05/14.
//  Copyright (c) 2014 Sonali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatCollectionHeaderView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *title;

@end
