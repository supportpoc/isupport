//
//  ACMAppleConnect.h
//  iPhoneAC
//
//  Created by Alexander Voloshyn and Volodymyr Prokurashko (shaman@apple.com) on 3/23/09.
//  Copyright 2009 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString* const kAppleConnectMobileErrorDomain;
enum
{
	kACCSuccess							        =  0,       /*kSSO_NoErr*/
	kACCErrorUserNotLoggedIn					= -100100,  /*kSSO_UserNotLoggedInErr*/
	kACCErrorUserLoginExpired					= -100101,  /*kSSO_UserLoginExpiredErr*/
	kACCErrorUserPasswordIncorrect				= -100102,  /*kSSO_UserPasswordIncorrectErr*/
	kACCErrorUserDoesNotExist					= -100103,  /*kSSO_UserDoesNotExistErr*/
	kACCErrorUserPasswordExpired				= -100104,  /*kSSO_UserPassworExpiredErr*/
	kACCErrorNoUsersLoggedIn					= -100105,  /*kSSO_NoUsersLoggedIn*/
    kACCErrorUserNewPasswordIncorrect			= -100106,  /*kSSO_UserNewPasswordIncorrectErr*/
	kACCErrorUserCredentialsMismatch			= -100110,  /*kSSO_UserCredentialsMismatchErr*/
	kACCErrorSignInAfterChangePassword			= -100111,  /*kSSO_SignInAfterChangePasswordErr*/
	kACCErrorUserAccountNameIncorrect			= -100115,  /*kSSO_UserAccountNameIncorrectErr*/
	kACCErrorUserCredentialsRevoked				= -100116,  /*kSSO_UserCredentialsRevokedErr*/
	kACCErrorUserAccountLocked					= -100117,  /*kSSO_UserAccountLockedErr*/
	kACCErrorUserAccountExpired					= -100118,  /*kSSO_UserAccountExpiredErr*/
	kACCErrorUserPasswordChanging				= -100119,  /*kSSO_UserPasswordChangingErr*/
	kACCErrorDeviceIdentityCertificateExpired __attribute__ ((deprecated))	= -100120,
	kACCErrorGestureUsedLastYear				= -100121,
	kACCErrorAuthenticationTokenExpired			= -100122,
	kACCErrorDeviceIdentityCertificateInvalid __attribute__ ((deprecated))	= -100123,
	kACCErrorPublicKeyInvalid					= -100124,
	kACCErrorRootCertificateIsNotInstalled __attribute__ ((deprecated))      = -100125,
	kACCErrorRealmInavalid						= -100160,  /*kSSO_RealmInavalidErr*/
	kACCErrorServerResponse						= -100360,  /*Incomplete server response*/
	kACCErrorNetworkIsNotAvailable				= -100401,  /*SNKP_AC_MACHINE_IS_OFFLINE_ERROR*/
 	kACCErrorNoInternetConnection               = -100402,  /*Device is not connected to internet */
	kACCErrorPersonIsNotInternalPerson			= -100500,	/*Person is not of internal person type */
	kACCErrorSciFiIsNotSetup					= -100501,
	kACCErrorSciFiIsNotAssociated				= -100502,
	kACCErrorModeNotSupportedForEnvironment		= -200030,  /*kSSO_ModeNotSupportedForEnvironmentErr*/
	kACCErrorFeatureNotImplemented				= -200040,  /*kSSO_FeatureNotImplementedErr*/
	kACCErrorUserCancel						    = -200050,  /*kSSO_UserCancelErr*/
    kACCErrorAppCancel						    = -200060,  /*kSSO_AppCancelErr*/
    kACCErrorSignInDialogIsAlreadyShown         = -200180,
	kACCErrorInvalidParam						= -200190,  /*kSSO_InvalidParamErr*/
	kACCErrorMisc							    = -200200,  /*kSSO_MiscErr*/
};

typedef enum
{
	kACMLogLevelOff					= 0,
	kACMLogLevelErrors,
	kACMLogLevelNotice,
	kACMLogLevelProfile,
	kACMLogLevelVerbose,
	kACMLogLevelDetail,
	kACMLogLevelCount
} ACMLogLevel;

typedef enum
{
	kACMContentStyleLight,
	kACMContentStyleDark,
} ACMContentStyle;

@protocol ACMBaseAuthenticationRequest;
@protocol ACMAuthenticationRequest;
@protocol ACMAuthenticationResponse;
@protocol ACMTicketVerificationRequest;
@protocol ACMTicketVerificationResponse;

/*!
    @class       ACMAppleConnect 
    @superclass  NSObject
    @abstract    AppleConnect Mobile controller class
    @discussion  Provides interface to authenticate user with AppleConnect and to verify user's service ticket
	@property (assign, nonatomic) id<ACMAppleConnectDelegate> delegate Delegate of the AppleConnect Mobile instance 
*/
@interface ACMAppleConnect : NSObject
{
@private
	id _private;
}

/*!
 @method     sharedInstance
 @result     singleton instance of AppleConnect Mobile class
 */
+ (ACMAppleConnect *)sharedInstance;

/*!
 @method     authenticate:
 @abstract   authenticates user
 @discussion authenticates user with parameters passed in request, if there is no ticket stored in keychain UI will be shown to authenticate
 @param      request is an object created using static methods of ACMRequest class, for instance + (id<ACMAuthenticationRequest>)authenticationRequest; should be used to create HTTP ticket request
 */
- (void)authenticate:(id<ACMBaseAuthenticationRequest>)request;

/*!
    @method     verifyServiceTicket:
    @abstract   Verifies ticket data
    @discussion Sends asynchronous ticket verification request to the server
	@param      request is an object created using static methods of ACMRequest class, for instance + (id<ACMTicketVerificationRequest>)ticketVerificationRequest; should be used to create HTTP ticket request
*/
- (void)verifyServiceTicket:(id<ACMTicketVerificationRequest>)aRequest;

/*!
 @method     logoutUser:inRealm:
 @abstract   log out
 @discussion this methid logs out specified user in specified realm
 @param      userName - AppleConnect ID
 @param      realm - AppleConnect realm (i.e. APPLECONNECT.APPLE.COM)
 */
- (void)logoutUser:(NSString*)userName inRealm:(NSString*)realm;

- (void)cancelRequests;

@property (assign, nonatomic) id delegate;
@property (readonly, nonatomic) NSString *version;
@property (assign, nonatomic) ACMLogLevel logLevel;
@end

/*!
 @protocol       ACMAppleConnectDelegate
 @abstract    delegate protocol to be implemented in order to be able to to get ticket or error if happened
 */
@protocol ACMAppleConnectDelegate<NSObject>

@required
- (UIViewController *)appleConnectParentViewController:(ACMAppleConnect *)appleConnect;

/*!
    @method     appleConnect:authenticationDidEndWithResponse:
    @abstract   Called by ACMAppleConect instance on the end of authentication process
    @discussion Response object contains result of the authentication. On success it contains ticket string, on failure - NSError object with failure error code (see error codes) and description string
    @param      appleConnect ACMAppleConnect instance
    @param      aResponse response object
*/
- (void)appleConnect:(ACMAppleConnect *)appleConnect authenticationDidEndWithResponse:(id<ACMAuthenticationResponse>)aResponse;

/*!
    @method     appleConnect:ticketVerificationDidEndWithResponse:
    @abstract   Called by ACMAppleConect instance on the end of ticket verification process
    @discussion Response object contains result of the ticket verification. On success it contains fullfiled session identification iformation (f.e. person DSID), on failure - NSError object with failure error code (see error codes) and description string
 @param      appleConnect ACMAppleConnect instance
 @param      aResponse response object
*/
- (void)appleConnect:(ACMAppleConnect *)appleConnect ticketVerificationDidEndWithResponse:(id<ACMTicketVerificationResponse>)aResponse;

@optional

/*!
 @method     appleConnectWillShowSignInDialog:
 @abstract   tells the delegate that the AppleConnect sign in dialog is going to be shown
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (void)appleConnectWillShowSignInDialog:(ACMAppleConnect *)appleConnect;

/*!
 @method     appleConnectDidShowSignInDialog:
 @abstract   tells the delegate that the AppleConnect sign in dialog is now shown
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (void)appleConnectDidShowSignInDialog:(ACMAppleConnect *)appleConnect;

/*!
 @method     appleConnectWillHideSignInDialog:
 @abstract   tells the delegate that the AppleConnect sign in dialog is going to be hidden
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (void)appleConnectWillHideSignInDialog:(ACMAppleConnect *)appleConnect;

/*!
 @method     appleConnectDidHideSignInDialog:
 @abstract   tells the delegate that the AppleConnect sign in dialog is now hidden
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (void)appleConnectDidHideSignInDialog:(ACMAppleConnect *)appleConnect;

/*!
 @method     appleConnectSignInAnimated:
 @abstract   indicates to the delegate if AppleConnect sign in dialog presents and dismisses itself with or w/o animation
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (BOOL)appleConnectSignInAnimated:(ACMAppleConnect *)appleConnect;

/*!
 @method     appleConnectSignInDialogContentStyle:
 @abstract   allows delegate to specify content style of SignIn dialog
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (ACMContentStyle)appleConnectSignInDialogContentStyle:(ACMAppleConnect *)appleConnect NS_AVAILABLE_IOS(7_0);

/*!
 @method     appleConnectManagerApprovalDialogSummaryView:
 @abstract   allows delegate to provide the custom view that will be composed at the top of Manager Approval screen. The view dimension should not exceed 320x88 points.
 @param      appleConnect - ACMAppleConnect shared instance
 */
- (UIView *)appleConnectManagerApprovalDialogSummaryView:(ACMAppleConnect *)appleConnect NS_AVAILABLE_IOS(7_0);

@end

#pragma mark Authentication

/*!
 @protocol     ACMBaseAuthenticationRequest <NSObject> 
 @abstract    Base authentication request
 @property ( retain) NSString* realm; - AppleConnect realm (i.e. APPLECONNECT.APPLE.COM)
 @property ( retain) NSString* userName; - AppleConnect ID
 @property ( retain) NSNumber* appID; - Assigned to application on registration with DS. Usually a small number, like 229.
 @property ( retain) NSString* appIDKey; - DS Application ID key. Required. Equivalent of application password, that is usually used on the backend to validate application. Long alphanumeric string. Assigned to the application upon registration with DS.
 @property ( retain) NSNumber* serverResponseTimeout; - timeout of the response from server, by default it's 60
 */
@protocol ACMBaseAuthenticationRequest <NSObject>
@property ( retain) NSString *realm;
@property ( retain) NSString *userName;
@property ( retain) NSNumber *appID;
@property ( retain) NSString *appIDKey;
@property ( retain) NSNumber *serverResponseTimeout;
@end

/*!
 @protocol     ACMAuthenticationRequest <ACMBaseAuthenticationRequest> 
 @abstract    Authentication request
 @discussion  Directly this protocol is not used, just contains base properties, if new ticket types added they will inherit properties from this base protocol
 @property ( retain) NSNumber *idleExpirationTimeout; - even if ticket is 8 hours it will be discarded and renewed after idleExpirationTimeout amount of time (in seconds)
 @property ( assign) BOOL localSingleSignOn; - ticket stored only in local keychain
 @property ( assign) BOOL disableSingleSignOn; - ticket is not stored anywhere 
 @property ( assign) BOOL managerSignIn; - authentication without storing ticket anywhere with different manager's UI
 @property ( assign) BOOL cancelAllowed; - by default is NO. If set to YES, AppleConnect Mobile sign in dialog will display a Cancel button, which allows user to cancel out of the sign in dialog if user doesn't wish to continue authentication process. After authentication/login screen will be closed AppleConnect will send response to delegate via API 'appleConnect:authenticationDidEndWithResponse:' with appropriate error code  (kACCErrorUserCancel = -200050).
 @property ( assign) BOOL disableGestureSignIn; - by default is NO. Starting ACM Internal v2.0 it is available to provide authentication via special user’s gesture. If ‘disableGestureSignIn’ property is set to YES, authentication via user’s gesture will be not available.
 @property ( retain) NSString *managerSignInPromptString; - If set, replaces default prompt on the managers sign in screen.
 */
@protocol ACMAuthenticationRequest <ACMBaseAuthenticationRequest>
@property ( assign) BOOL localSingleSignOn __attribute__ ((deprecated));
@property ( assign) BOOL disableSingleSignOn;
@property ( assign) BOOL managerSignIn;
@property ( assign) BOOL cancelAllowed;
@property ( assign) BOOL disableGestureSignIn __attribute__ ((deprecated));
@property ( retain) NSString *managerSignInPromptString;
@property ( retain) NSNumber *idleExpirationTimeout;
@end

/*!
 @protocol       ACMAuthenticationResponse <NSObject> 
 @abstract    This protocol describes response after authentication ended
 @property ( retain, readonly) NSString *token; - retrieved token
 @property ( retain, readonly) NSString *userName; - user name for whom token was retrieved
 @property ( retain, readonly) NSDictionary *rawResponseData; - contains complete server response dictionary
 @property ( retain, readonly) NSError *error; - nil if ticket retrieved successfully or NSError if error happened
 */
@protocol ACMAuthenticationResponse <NSObject>
@property ( retain, readonly) NSString *token;
@property ( retain, readonly) NSString *userName;
@property ( retain, readonly) NSDictionary *rawResponseData;
@property ( retain, readonly) NSError *error;
@end

#pragma mark -
#pragma mark Ticket validation

/*!
 @protocol    ACMTicketVerificationRequest <NSObject>
 @abstract    Ticket verification request
 @property ( retain) NSString *userName; - User name for whom token was retrieved
 @property ( retain) NSString* realm; - AppleConnect realm (i.e. APPLECONNECT.APPLE.COM)
 @property ( retain) NSString* token; - User's token string
 @property ( retain) NSString* appIDKey; - DS Application ID key. Required. Equivalent of application password, that is usually used on the backend to validate application. Long alphanumeric string. Assigned to the application upon registration with DS.
 @property ( retain) NSNumber *appID; - Assigned to application on registration with DS. Usually a small number, like 229.
 @property ( retain) NSNumber* serverResponseTimeout; - Timeout of the response from server, by default it's 60
 @property ( retain) id userInfo; - Optional. This parameter is used to pass any additional parameters.
 */
@protocol ACMTicketVerificationRequest <NSObject>
@property ( retain) NSString *userName;
@property ( retain) NSString *realm;
@property ( retain) NSString *token;
@property ( retain) NSString *appIDKey;
@property ( retain) NSNumber *appID;
@property ( retain) NSNumber* serverResponseTimeout;
@property ( retain) id userInfo;
@end

/*!
 @protocol    ACMTicketVerificationResponse <NSObject>
 @abstract    This protocol describes response on ticket verification request
 @property ( retain, readonly) NSNumber *personDSID; - on success contains person ID assiciated with corresponding user
 @property ( retain, readonly) NSDictionary* rawResponseData; - contains complete server response dictionary
 @property ( retain, readonly) NSError* error; - nil on success or NSError otherwise
 @property ( retain, readonly) id userInfo; - this parameter is used to pass any additional parameters.
 */
@protocol ACMTicketVerificationResponse <NSObject>
@property ( retain, readonly) NSNumber *personDSID;
@property ( retain, readonly) NSDictionary *rawResponseData;
@property ( retain, readonly) NSError* error;
@property ( retain, readonly) id userInfo;
@end

#pragma mark -
#pragma mark Creating Requests

/*!
 @class       ACMRequest 
 @superclass  NSObject
 @abstract    Factory interface for all AC Mobile requests
 */
@interface ACMRequest : NSObject
{
}
/*!
 @method     authenticationRequest
 @abstract   creates authentication request instance
 @discussion creates empty authentication request which conforms to ACMAuthenticationRequest protocol, this is the only way to create authentication request for method authenticate:
 @result     object which conforms to ACMAuthenticationRequest protocol
 */
+ (id)authenticationRequest;

/*!
 @method     ticketVerificationRequest
 @abstract   creates ticket verifiction request instance
 @discussion creates empty ticket verifiction request which conforms to ACMTicketVerificationRequest protocol, this is the only way to create authentication request for method verifyServiceTicket:
 @result     object which conforms to ACMTicketVerificationRequest protocol
 */
+ (id)ticketVerificationRequest;
@end
