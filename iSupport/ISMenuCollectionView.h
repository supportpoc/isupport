//
//  ISMenuCollectionView.h
//  iSupport_PROT_1
//
//  Created by Ankit on 03/06/14.
//  Copyright (c) 2014 Ankit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISMenuCollectionViewDataSource.h"
#import "ISMenuCollectionViewDelegate.h"


@interface ISMenuCollectionView : UICollectionView<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>{
    int pageCount;
    BOOL pageControlBeingUsed;
}

@property (nonatomic, strong) NSArray *menuOptionsArray;
@property (nonatomic, strong) NSString *menuOptionIconName;
@property (nonatomic, strong) NSArray *menuIconsArray;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@property (nonatomic, assign) IBOutlet id<ISMenuCollectionViewDataSource> menuOptionsDatasource;
@property (nonatomic, assign) IBOutlet id<ISMenuCollectionViewDelegate> menuOptionsDelegate;

-(id) initWithMenuOptions:(NSArray*) menuOptions;
@end
