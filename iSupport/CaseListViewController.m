//
//  CaseListViewController.m
//  CustomerExperience
//
//  Created by Ila on 27/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "CaseListViewController.h"
#import "CaseTableViewCell.h"
#import "RESideMenu.h"
#import "MGSwipeButton.h"

#define TEST_USE_MG_DELEGATE 1


@interface CaseListViewController ()


@property(nonatomic, strong) NSMutableArray *devices;
@property(nonatomic, strong) NSMutableArray *cases;


@end

@implementation CaseListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.view.backgroundColor  = [UIColor colorWithRed:233.0/255 green:233.0/255.0 blue:233.0/255.0 alpha:1];
    //    productTable.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1];
    //   self.devices = [[NSMutableArray alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DashboardPlist" ofType:@"plist"];
    NSDictionary *myProducts = [NSDictionary dictionaryWithContentsOfFile:path];
    
    NSArray *caseList = [myProducts objectForKey:@"MyCases"];
    self.cases = [[NSMutableArray alloc]initWithArray:caseList];
    
    
    
    
    /* Navigation Bar */
    
    self.navigationItem.title = @"Case List";
    if(!self.isPushed){

    UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(showMenu:)];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    }
    
    UIImage *addImage = [UIImage imageNamed:@"menu-add-devices.png"];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithImage:addImage
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:nil];
    self.navigationItem.rightBarButtonItem = addButton;

    //    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //
}

- (void)showMenu:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //        return [self.details count];
    return [self.cases count];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 141;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *myCase =  [self.cases objectAtIndex:indexPath.row];

    static NSString *popIdentifier = @"CEProductTableViewCell";
    self.caseCell = [tableView dequeueReusableCellWithIdentifier:popIdentifier];
    [self.caseCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if(self.caseCell == nil){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CaseTableViewCell" owner:self options:nil];
        self.caseCell = [nib objectAtIndex:0];
    }
    self.caseCell.productImage.image = [UIImage imageNamed:[myCase objectForKey:@"productImage"]];
    self.caseCell.productName.text = [myCase objectForKey:@"productName"];
    self.caseCell.serialNumber.text = myCase[@"serialNumber"];
    self.caseCell.dateCreated.text = [myCase objectForKey:@"dateCreated"];
    self.caseCell.caseStatus.text = [myCase objectForKey:@"caseStatus"];
    self.caseCell.caseStatus.layer.cornerRadius = 8;
    self.caseCell.caseStatus.layer.masksToBounds = YES;
    self.caseCell.problem.text = [myCase objectForKey:@"problem"];
    self.caseCell.probDescription.text = [myCase objectForKey:@"probDescription"];

    if([[myCase objectForKey:@"caseStatus"] isEqualToString:@"Open" ]){
        self.caseCell.caseStatus.backgroundColor = [UIColor orangeColor];
    }
    else{
        self.caseCell.caseStatus.backgroundColor = [UIColor colorWithRed:75.0/255.0 green:186.0/255.0 blue:62.0/255.0 alpha:1.0];
    }
//     self.caseCell.problem.text = [myCase objectForKey:@"problem"];
//     self.caseCell.probDescription.text = [myCase objectForKey:@"probDescripton"];
        self.caseCell.delegate = self;
    self.caseCell.rightButtons = [self createRightButtons:1];
    return self.caseCell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    //[self showGrid];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

-(BOOL) swipeTableCell:(CaseTableViewCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        NSIndexPath * path = [caseTable indexPathForCell:cell];
        [self.cases removeObjectAtIndex:path.row];
        [caseTable deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
    }
    
    return YES;
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    NSString* titles = @"Delete";
    UIColor * colors = [UIColor redColor];
    for (int i = 0; i < number; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles backgroundColor:colors callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (right).");
            return YES;
        }];
        [result addObject:button];
    }
    return result;
}

- (void)showGrid {
    NSInteger numberOfOptions = 4;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"create-a-case"] title:@"Create A Case"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"setup-a-repair"] title:@"Setup A Repair"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"take-in-service-circle"] title:@"Take In Service"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"extend-service-support-coverage"] title:@"Extend Support"],
                       
                       ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    
    // RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:161.0/255.0 blue:235.0/255.0 alpha:1.0 ];
    av.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow_bg"]];
    av.itemSize = CGSizeMake(150,150);
    av.blurLevel = 0.1;
    // av.itemFont = [UIFont boldSystemFontOfSize:10];
    
    //    av.bounces = NO;
    //[av showViewController:self sender:nil];
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
}



  
@end
