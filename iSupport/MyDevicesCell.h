//
//  MyDevicesCell.h
//  iSupportDemo
//
//  Created by Varun on 27/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDevicesCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UILabel *productNumber;
@property (strong, nonatomic) IBOutlet UILabel *totalDevices;
@property (strong, nonatomic) IBOutlet UIScrollView *myDevicesScrollView;






@end
