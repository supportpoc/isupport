//
//  ViewController.h
//  iSupportTest
//
//  Created by Varun on 27/11/14.
//  Copyright (c) 2014 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDevicesCell.h"
#import "MyCasesCell.h"
#import "MyRepairsCell.h"
#import "ISMenuCollectionView.h"
#import "RNGridMenu.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ISConstants.h"


@interface DeviceDashboardController : UIViewController <UITableViewDelegate, UITableViewDataSource, ISMenuCollectionViewDataSource, ISMenuCollectionViewDelegate,RNGridMenuDelegate,MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) NSDictionary *myProducts;
@property (strong, nonatomic) MyCasesCell *myCaseCell;
@property (strong, nonatomic) MyDevicesCell *myDeviceCell;
@property (strong, nonatomic) MyRepairsCell *myRepairCell;
@property (nonatomic, strong) IBOutlet ISMenuCollectionView *menuCollectionView;

- (IBAction)showMainMenuBtnAction:(id)sender;

@end


