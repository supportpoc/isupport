//
//  ISRootViewController.m
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ISRootViewController.h"
#import "ISSideMenuViewController.h"

@interface ISRootViewController ()

@end

@implementation ISRootViewController

- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];

    self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    
//    EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//    /* User Personalized Screen */
//    [super.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:[weatherManager userPersonalizedScreenImageName]]]];
    
    [super.view setBackgroundColor:[UIColor blackColor]];
    self.delegate = ( ISSideMenuViewController*)self.menuViewController;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if ( event.subtype == UIEventSubtypeMotionShake )
    {
        // Put in code here to handle shake
//        [self launchCreateNoteScreen];
    }
}
//- (void) launchCreateNoteScreen {
//    
//    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EMCreateNoteViewController *cnVC = (EMCreateNoteViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"EMCreateNoteViewController"];
//    
//    [self presentViewController:cnVC animated:YES completion:^{
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
//    }];
//}

@end
