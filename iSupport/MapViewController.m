//
//  MapViewController.m
//  iSupport
//
//  Created by Ankur on 28/11/14.
//  Copyright (c) 2014 infogain. All rights reserved.
//

#import "MapViewController.h"
#import "MapViewAnnotation.h"
#import "RESideMenu.h"

@interface MapViewController (){

    UIView *customView;
    NSString *currentLati;
    NSString *currentLongi;
    NSString *currentTitle;
    
}

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [mapView addAnnotations:[self createAnnotations]];
    
    NSLog(@"_mapPinsArray %@",_mapPinsArray);

    // Do any additional setup after loading the view.
    UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(didTapMap:)];
    [mapView addGestureRecognizer:tapRec];
    
    self.navigationItem.title = @"Apple Service Centers";
    
    if (self.isFromSideMenu)
    {
        UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(showMenu:)];
        self.navigationItem.leftBarButtonItem = menuButton;
    }

}


-(void)didTapMap :(UITapGestureRecognizer *)tapview{

    [customView removeFromSuperview];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (NSMutableArray *)createAnnotations
{
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    //Read locations details from plist
   // NSString *path = [[NSBundle mainBundle] pathForResource:@"locations" ofType:@"plist"];
   // NSArray *locations = [NSArray arrayWithContentsOfFile:path];
    
    for (NSDictionary *row in _mapPinsArray) {
        NSNumber *latitude = [row objectForKey:@"latitude"];
        NSNumber *longitude = [row objectForKey:@"longitude"];
        NSString *title = [row objectForKey:@"Title"];
        //Create coordinates from the latitude and longitude values
        CLLocationCoordinate2D coord;
        coord.latitude = latitude.doubleValue;
        coord.longitude = longitude.doubleValue;
        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:title AndCoordinate:coord];
        [annotations addObject:annotation];
    }
    return annotations;
}







#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.15
#define MAX_DEGREES_ARC 360
//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapViewLocal animated:(BOOL)animated
{
    NSArray *annotations = mapViewLocal.annotations;
    int count = [mapViewLocal.annotations count];
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [mapViewLocal setRegion:region animated:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self zoomMapViewToFitAnnotations:mapView animated:animated];
    //or maybe you would do the call above in the code path that sets the annotations array
}


- (void)mapView:(MKMapView *)mapViewLocal didSelectAnnotationView:(MKAnnotationView *)view
{
    
    for (id currentAnnotation in mapViewLocal.annotations) {
        if ([currentAnnotation isKindOfClass:[MapViewAnnotation class]]) {
            [mapViewLocal deselectAnnotation:currentAnnotation animated:YES];
        }
    }
    /*
    for (id currentAnnotation in mapViewLocal.annotations) {
        if ([currentAnnotation isKindOfClass:[MapViewAnnotation class]]) {
            [mapViewLocal deselectAnnotation:currentAnnotation animated:YES];
        } 
    }
    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {

    customView=[[UIView alloc]initWithFrame:CGRectMake(view.center.x-106, view.center.y-70, 200, 50)];
    customView.backgroundColor=[UIColor greenColor];
        [view.superview addSubview:customView];
    
    }
    
   */
    /*
   // view.canShowCallout=NO;
    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {
        CGSize  calloutSize = CGSizeMake(100.0, 80.0);
        UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y-calloutSize.height, calloutSize.width, calloutSize.height)];
        calloutView.backgroundColor = [UIColor whiteColor];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(5.0, 5.0, calloutSize.width - 10.0, calloutSize.height - 10.0);
        [button setTitle:@"OK" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(checkin) forControlEvents:UIControlEventTouchUpInside];
        [calloutView addSubview:button];
        [view.superview addSubview:calloutView];
    }
    */
}


- (void)mapView:(MKMapView *)mapViewLocal didDeselectAnnotationView:(MKAnnotationView *)view{

    
   // NSLog(@"view.annotation.title %@",view.annotation.title);
   // NSLog(@"view.annotation.title %@",view.annotation.title);

    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {
        
        [self createCustomViewfromView:view];
    }


}



-(void)createCustomViewfromView:(MKAnnotationView *)view{

    
    
    customView=[[UIView alloc]initWithFrame:CGRectMake(view.center.x-106, view.center.y-70, 200, 80)];
    customView.backgroundColor=[UIColor whiteColor];
    
    UILabel *maplabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 5, 160, 19)];
    maplabel.backgroundColor=[UIColor clearColor];
    [maplabel setFont:[UIFont boldSystemFontOfSize:15]];
    maplabel.text=[view.annotation.title uppercaseString];
    [customView addSubview:maplabel];
    
    
    UILabel *mapDescription=[[UILabel alloc]initWithFrame:CGRectMake(5, 25, 160, 31)];
    mapDescription.backgroundColor=[UIColor clearColor];
    mapDescription.numberOfLines=2;
    [mapDescription setFont:[UIFont systemFontOfSize:12]];
    
    for (NSDictionary *row in _mapPinsArray) {
        NSString *title = [row objectForKey:@"Title"];

        if ([title isEqualToString:view.annotation.title]) {
            currentLati=[row objectForKey:@"latitude"];
            currentLongi=[row objectForKey:@"longitude"];
            currentTitle=[row objectForKey:@"Title"];
            mapDescription.text= [row objectForKey:@"Description"];
        }
    }
    
    mapDescription.textColor=[UIColor darkGrayColor];
    [customView addSubview:mapDescription];
    
    
    UIButton *directionbtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    directionbtn.frame = CGRectMake(170.0, 5.0, 20, 20);
    [directionbtn setBackgroundImage:[UIImage imageNamed:@"location_icon"] forState:UIControlStateNormal];
    [directionbtn addTarget:self action:@selector(directionBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:directionbtn];

    
    UIButton *globebtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    globebtn.frame = CGRectMake(170.0, 30.0, 20, 20);
    [globebtn setBackgroundImage:[UIImage imageNamed:@"globe_icon"] forState:UIControlStateNormal];
    [globebtn addTarget:self action:@selector(globeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:globebtn];
    
    customView.layer.cornerRadius=7;
    [customView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [customView.layer setBorderWidth:0.5f];
    [view.superview addSubview:customView];
    
    UIButton *setupAppointment = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    setupAppointment.frame = CGRectMake(2.0, 57.0, 100, 20);
    [setupAppointment setTitle:@"Setup Appointment" forState:UIControlStateNormal];
    setupAppointment.titleLabel.font = [UIFont systemFontOfSize:10];
    [customView addSubview:setupAppointment];
    
    UIButton *repair = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    repair.frame = CGRectMake(100.0, 57.0, 100, 20);
    [repair setTitle:@"Bring For Repair" forState:UIControlStateNormal];
    repair.titleLabel.font = [UIFont systemFontOfSize:10];
    [customView addSubview:repair];
}

-(void)directionBtnClicked{

/*
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(37.776142, -122.424774) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake([currentLati doubleValue], [currentLongi doubleValue]) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSLog(@"response = %@",response);
        NSArray *arrRoutes = [response routes];
        [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            MKRoute *rout = obj;
            
            MKPolyline *line = [rout polyline];
            [mapView addOverlay:line];
            NSLog(@"Rout Name : %@",rout.name);
            NSLog(@"Total Distance (in Meters) :%f",rout.distance);
            
            NSArray *steps = [rout steps];
            
            NSLog(@"Total Steps : %d",[steps count]);
            
            [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSLog(@"Rout Instruction : %@",[obj instructions]);
                NSLog(@"Rout Distance : %f",[obj distance]);
            }];
        }];
    }];
    */
    
    CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake([currentLati doubleValue],[currentLongi doubleValue]);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
    MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
    item.name = currentTitle;
    [item openInMapsWithLaunchOptions:nil];
    
    /*
    NSString* url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=Current+Location&daddr=%f,%f", [currentLati doubleValue], [currentLongi doubleValue]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    */
    
    
}
/*
- (MKOverlayView *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}


- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id)overlay {
    
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}
*/

-(void)globeBtnClicked{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.apple.com"]];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    MKAnnotationView *aV;
    for (aV in views) {
        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
            MKAnnotationView* annotationView = aV;
            annotationView.canShowCallout = NO;
            
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    NSLog(@"regionWillChangeAnimated");
     [customView removeFromSuperview];

}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

    NSLog(@"regionDidChangeAnimated");

}

- (void)showMenu:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}



@end
